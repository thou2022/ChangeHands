//屏蔽console.log打印语句
//console.log = ()=>{}

//获取域名+端口号的根URL
function getRootURL() {
	//return window.location.protocol+'//'+window.location.host;
	return 'http://localhost:8080/';
	//return 'http://www.anneli.cn/';
}

//重写alert
window.alert = function(msg, callback) {
	var msg = msg.toString()
	
	layui.use('layer', function() {
		var layer = layui.layer;
		layer.ready(function() {
			layer.alert(msg, {
				title: '提示',
				skin: 'layui-layer-molv', //样式类名  自定义样式
				closeBtn: 1, // 是否显示关闭按钮
				anim: 1, //动画类型
				btn: ['确认'], //按钮
				icon: 6, // icon
				success: function() {
					this.enterEsc = function(event) {
						if (event.keyCode === 13) {
							$(".layui-layer-btn0").click();
							return false; //阻止系统默认回车事件
						} else if (event.keyCode == 27) {
							$(".layui-layer-btn1").click();
							return false;
						}
					};
					$(document).on('keydown', this.enterEsc); //监听键盘事件，关闭层
				},
				yes: function(index) {
					if (typeof callback == 'function') {
						callback();
					}
					layer.close(index);
				},
				end: function() {
					$(document).off('keydown', this.enterEsc); //解除键盘关闭事件
				}
			})
		})
	})
}

function reload(){
	layui.use(['element','form'], function(){
		  //var element = layui.element(); 
		  var form = layui.form();
		  //element.init(); //这行代码就是关键
		  form.render();
	});
}

//判断是否是数字
function isRealNum(val, msg) {
	// isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除，    
	if (val === "" || val == null) {
		alert(msg);
		return false;
	}
	if (!isNaN(val)) {
		//对于空数组和只有一个数值成员的数组或全是数字组成的字符串，isNaN返回false，例如：'123'、[]、[2]、['123'],isNaN返回false,   //所以如果不需要val包含这些特殊情况，则这个判断改写为if(!isNaN(val) && typeof val === 'number' )
		return true;
	} else {
		alert(msg);
		return false;
	}
}

//空值效验 有提示
function validityForStringNull(val, msg) {
	if (val == null || val === '') {
		alert(msg);
		return true;
	} else {
		return false;
	}
}

//等值效验 转小写
function validityForStringEqual(val1, val2) {
	console.log(val1.toLowerCase() + " " + val2.toLowerCase())
	if (val1.toLowerCase() === val2.toLowerCase()) {
		return true;
	} else {
		return false;
	}
}

//把时间戳转换日期格式
function timeStamp2String(time) {
	var datetime = new Date();
	datetime.setTime(time);
	var year = datetime.getFullYear();
	var month = datetime.getMonth() + 1 < 10 ? "0" + (datetime.getMonth() + 1) : datetime.getMonth() + 1;
	var date = datetime.getDate() < 10 ? "0" + datetime.getDate() : datetime.getDate();
	var hour = datetime.getHours() < 10 ? "0" + datetime.getHours() : datetime.getHours();
	var minute = datetime.getMinutes() < 10 ? "0" + datetime.getMinutes() : datetime.getMinutes();
	var second = datetime.getSeconds() < 10 ? "0" + datetime.getSeconds() : datetime.getSeconds();
	return year + "-" + month + "-" + date + " " + hour + ":" + minute + ":" + second;
}

/* 下面为验证码生成过程 */

//验证码全局变量
var verVal;

function checknum(imgname, canvasname) {
	var nums = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0",
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
		'X', 'Y', 'Z',
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
		'x', 'y', 'z'
	];

	var checknum_imgname = '';
	var checknum_canvasname = '';

	/**
	 * 此方法用于生成二维码 
	 * @return  生成的二维码字符串
	 * @param {图片id} imgname 
	 * @param {canvas 的id} canvasname 
	 */
	function initChecknum(imgname, canvasname) {
		checknum_imgname = imgname
		checknum_canvasname = canvasname;

		resetCode();
		// verVal = drawCode();
		return verVal;
	}
	return initChecknum(imgname, canvasname);
	// 绘制验证码
	function drawCode(str) {
		var canvas = document.getElementById(checknum_canvasname); //获取HTML端画布
		var context = canvas.getContext("2d"); //获取画布2D上下文
		context.fillStyle = "#f2feea"; //画布填充色
		context.fillRect(0, 0, canvas.width, canvas.height); //清空画布
		context.fillStyle = "#4aa32a"; //设置字体颜色
		context.font = "90px Arial"; //设置字体
		var rand = new Array();
		var x = new Array();
		var y = new Array();
		for (var i = 0; i < 4; i++) {
			rand.push(rand[i]);
			rand[i] = nums[Math.floor(Math.random() * nums.length)]
			x[i] = i * 60 + 30;
			y[i] = Math.random() * 30 + 90;
			context.fillText(rand[i], x[i], y[i]);
		}
		str = rand.join('').toUpperCase();
		//画3条随机线
		for (var i = 0; i < 3; i++) {
			drawline(canvas, context);
		}

		// 画30个随机点
		for (var i = 0; i < 30; i++) {
			drawDot(canvas, context);
		}
		convertCanvasToImage(canvas);
		return str;
	}

	// 随机线
	function drawline(canvas, context) {
		context.moveTo(Math.floor(Math.random() * canvas.width), Math.floor(Math.random() * canvas.height)); //随机线的起点x坐标是画布x坐标0位置，y坐标是画布高度的随机数
		context.lineTo(Math.floor(Math.random() * canvas.width), Math.floor(Math.random() * canvas.height)); //随机线的终点x坐标是画布宽度，y坐标是画布高度的随机数
		context.lineWidth = 0.5; //随机线宽
		context.strokeStyle = 'rgba(50,50,50,0.3)'; //随机线描边属性
		context.stroke(); //描边，即起点描到终点
	}
	// 随机点(所谓画点其实就是画1px像素的线，方法不再赘述)
	function drawDot(canvas, context) {
		var px = Math.floor(Math.random() * canvas.width);
		var py = Math.floor(Math.random() * canvas.height);
		context.moveTo(px, py);
		context.lineTo(px + 1, py + 1);
		context.lineWidth = 0.2;
		context.stroke();

	}
	// 绘制图片
	function convertCanvasToImage(canvas) {
		document.getElementById("verifyCanvas").style.display = "none";
		var image = document.getElementById(checknum_imgname);
		image.src = canvas.toDataURL("image/png");
		return image;
	}

	// 点击图片刷新
	function resetCode() {
		var verifyCanvas = document.getElementById(checknum_canvasname)
		if (verifyCanvas && verifyCanvas.parentNode) {
			verifyCanvas.parentNode.removeChild(verifyCanvas);
		}
		// $('#verifyCanvas').remove();
		//$('#code_img').before('<canvas width="100" height="40" id="verifyCanvas"></canvas>')
		verifyCanvas = document.createElement("canvas");
		verifyCanvas.id = checknum_canvasname;
		var img = document.getElementById(checknum_imgname);
		img.parentNode.insertBefore(verifyCanvas, img);
		verVal = drawCode();
		return verVal;
	}
}
/* 验证码生成过程end */
