package com.chs.config;

import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

//spring boot 注册组件
//@Configuration
public class WebSocketConfig implements WebMvcConfigurer{
    @Bean
    public ServerEndpointExporter serverEndpointExporter(){
    	//注册异常组件
        return new ServerEndpointExporter();
    }
}
