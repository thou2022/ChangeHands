package com.chs.dao;

import com.chs.model.Product;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;
import java.util.List;

/**
 * (Product)表数据库访问层
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:38
 */
 @Mapper
public interface ProductMapper {

    /**
     * 通过商品ID查询单条商品数据
     *
     * @param pid 主键
     * @return 实例对象
     */
    Product selectByPrimaryKey(String pid);

    /**
     * 查询商品记录列表
     *
     * @param product 实例对象
     * @return 对象列表
     */
    List<Product> selectAll();
    
    List<Product> selectAll(Product product);
    
    List<Product> selectAllForUser();

    /**
     * 新增商品数据
     *
     * @param product 实例对象
     * @return 影响行数
     */
    int insertSelective(Product product);

    /**
     * 管理员修改数据
     *
     * @param product 实例对象
     * @return 影响行数
     */
    int updateByPrimaryKeySelective(Product product);

    /**
     * 通过主键（商品id）删除数据
     *
     * @param pid 主键
     * @return 影响行数
     */
    int deleteByPrimaryKey(String pid);
    
    /**
     *批量删除商品
     * @param uids
     */
    int  batchdeleteProduct(ArrayList<String> list);
    
    /**
     * 得到商品总数
     * @return
     */
    int getProductCount();
    
    /**
     * 模糊查询商品
     */
    List<Product> findProductByList(String pp);
}