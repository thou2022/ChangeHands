package com.chs.dao;

import com.chs.model.Admin;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * (Admin)表数据库访问层
 *
 * @author mopeiwen
 * @since 2020-06-10 10:47:19
 */
 @Mapper
public interface AdminMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param aid 主键
     * @return 实例对象
     */
    Admin selectByPrimaryKey(String aid);
    
    /**
     * 通过账号来实现管理员登录
     * @param account
     * @param password
     * @return
     */
    Admin selectByAccount(String account,String password);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param admin 实例对象
     * @return 对象列表
     */
    List<Admin> selectAll(Admin admin);

    /**
     * 新增数据
     *
     * @param admin 实例对象
     * @return 影响行数
     */
    int insertSelective(Admin admin);

    /**
     * 修改数据
     *
     * @param admin 实例对象
     * @return 影响行数
     */
    int updateByPrimaryKeySelective(Admin admin);

    /**
     * 通过主键删除数据
     *
     * @param aid 主键
     * @return 影响行数
     */
    int deleteByPrimaryKey(String aid);

}