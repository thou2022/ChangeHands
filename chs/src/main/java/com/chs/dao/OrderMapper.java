package com.chs.dao;

import com.chs.model.Order;

import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;
import java.util.List;

/**
 * (Order)表数据库访问层
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:17
 */
 @Mapper
public interface OrderMapper {

    /**
     * 管理员通过订单ID查询单条订单数据
     *
     * @param oid 主键
     * @return 实例对象
     */
    Order selectByPrimaryKey(String oid);

    /**
     * 管理员查询所有订单列表
     *
     * @param order 实例对象
     * @return 对象列表
     */
    List<Order> selectAll();
    
    List<Order> selectAll(Order order);
    
    List<Order> selectAllMySold(String uid);

    /**
     * 新增数据
     *
     * @param order 实例对象
     * @return 影响行数
     */
    int insertSelective(Order order);

    /**
     * 修改数据
     *
     * @param order 实例对象
     * @return 影响行数
     */
    int updateByPrimaryKeySelective(Order order);

    /**
     * 管理员通过主键（订单id）删除数据
     *
     * @param oid 主键
     * @return 影响行数
     */
    int deleteByPrimaryKey(String oid);
    
    /**
     *批量删除订单用户
     * @param uids
     */
    int  batchdeleteOrder(ArrayList<String> list);
    
    /**
     * 得到订单总数
     * @return
     */
    int getOrderCount();

    /**
     * 模糊查询订单
     */
    List<Order> findOrderByList(String oo);
}
