package com.chs.dao;

import com.chs.model.ShoppingCart;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * (ShoppingCart)表数据库访问层
 *
 * @author mopeiwen
 * @since 2020-06-10 10:49:06
 */
 @Mapper
public interface ShoppingCartMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param scid 主键
     * @return 实例对象
     */
    ShoppingCart selectByPrimaryKey(String scid);
    
    ShoppingCart selectByPid(String pid);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param shoppingCart 实例对象
     * @return 对象列表
     */
    List<ShoppingCart> selectAll(ShoppingCart shoppingCart);

    /**
     * 新增数据
     *
     * @param shoppingCart 实例对象
     * @return 影响行数
     */
    int insertSelective(ShoppingCart shoppingCart);

    /**
     * 修改数据
     *
     * @param shoppingCart 实例对象
     * @return 影响行数
     */
    int updateByPrimaryKeySelective(ShoppingCart shoppingCart);

    /**
     * 通过主键删除数据
     *
     * @param scid 主键
     * @return 影响行数
     */
    int deleteByPrimaryKey(String scid);

}