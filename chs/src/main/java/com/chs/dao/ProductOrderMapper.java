package com.chs.dao;

import com.chs.model.ProductOrder;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * (ProductOrder)表数据库访问层
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:57
 */
 @Mapper
public interface ProductOrderMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param poid 主键
     * @return 实例对象
     */
    ProductOrder selectByPrimaryKey(String poid);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param productOrder 实例对象
     * @return 对象列表
     */
    List<ProductOrder> selectAll(ProductOrder productOrder);

    /**
     * 新增数据
     *
     * @param productOrder 实例对象
     * @return 影响行数
     */
    int insertSelective(ProductOrder productOrder);

    /**
     * 修改数据
     *
     * @param productOrder 实例对象
     * @return 影响行数
     */
    int updateByPrimaryKeySelective(ProductOrder productOrder);

    /**
     * 通过主键删除数据
     *
     * @param poid 主键
     * @return 影响行数
     */
    int deleteByPrimaryKey(String poid);

}