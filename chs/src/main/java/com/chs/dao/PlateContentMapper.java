package com.chs.dao;

import com.chs.model.PlateContent;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * (PlateContent)表数据库访问层
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:27
 */
 @Mapper
public interface PlateContentMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param pcid 主键
     * @return 实例对象
     */
    PlateContent selectByPrimaryKey(String pcid);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param plateContent 实例对象
     * @return 对象列表
     */
    List<PlateContent> selectAll();

    /**
     * 新增数据
     *
     * @param plateContent 实例对象
     * @return 影响行数
     */
    int insertSelective(PlateContent plateContent);

    /**
     * 修改数据
     *
     * @param plateContent 实例对象
     * @return 影响行数
     */
    int updateByPrimaryKeySelective(PlateContent plateContent);

    /**
     * 通过主键删除数据
     *
     * @param pcid 主键
     * @return 影响行数
     */
    int deleteByPrimaryKey(String pcid);

}