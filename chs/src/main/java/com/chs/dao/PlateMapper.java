package com.chs.dao;

import com.chs.model.Plate;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * (Plate)表数据库访问层
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:21
 */
 @Mapper
public interface PlateMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param pid 主键
     * @return 实例对象
     */
    Plate selectByPrimaryKey(String pid);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param plate 实例对象
     * @return 对象列表
     */
    List<Plate> selectAll();

    /**
     * 管理员新增板块数据
     * @param plate 实例对象
     * @return 影响行数
     */
    int insertSelective(Plate plate);

    /**
     * 修改数据
     *
     * @param plate 实例对象
     * @return 影响行数
     */
    int updateByPrimaryKeySelective(Plate plate);

    /**
     * 通过主键删除数据
     *
     * @param pid 主键
     * @return 影响行数
     */
    int deleteByPrimaryKey(String pid);

}