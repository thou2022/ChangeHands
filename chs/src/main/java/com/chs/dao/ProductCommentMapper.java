package com.chs.dao;

import com.chs.model.ProductComment;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;
import java.util.List;

/**
 * (ProductComment)表数据库访问层
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:49
 */
 @Mapper
public interface ProductCommentMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param pcid 主键
     * @return 实例对象
     */
    ProductComment selectByPrimaryKey(String pcid);

    /**
     * 管理员通过实体作为筛选条件查询所有评论列表
     *
     * @param productComment 实例对象
     * @return 对象列表
     */
    List<ProductComment> selectAll();
    
    List<ProductComment> selectAll(ProductComment productComment);
    
    List<ProductComment> selectMyAll(ProductComment productComment);

    /**
     * 新增数据
     *
     * @param productComment 实例对象
     * @return 影响行数
     */
    int insertSelective(ProductComment productComment);
    
    /**
     * 修改数据
     *
     * @param productComment 实例对象
     * @return 影响行数
     */
    int updateByPrimaryKeySelective(ProductComment productComment);

    /**
     * 通过主键删除数据
     *
     * @param pcid 主键
     * @return 影响行数
     */
    int deleteByPrimaryKey(String pcid);
    
    /**
     *批量删除评论
     * @param pcids
     */
    int  batchdeleteComment(ArrayList<String> list);
    
    /**
     * 模糊查询商品评论
     */
    List<ProductComment> findCommentByList(String cc);
}