package com.chs.dao;

import com.chs.model.PlateType;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * (PlateType)表数据库访问层
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:32
 */
 @Mapper
public interface PlateTypeMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param pyid 主键
     * @return 实例对象
     */
    PlateType selectByPrimaryKey(String pyid);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param plateType 实例对象
     * @return 对象列表
     */
    List<PlateType> selectAll();

    /**
     * 新增板块类型数据
     *
     * @param plateType 实例对象
     * @return 影响行数
     */
    int insertSelective(PlateType plateType);

    /**
     * 修改板块类型数据
     *
     * @param plateType 实例对象
     * @return 影响行数
     */
    int updateByPrimaryKeySelective(PlateType plateType);

    /**
     * 通过主键删除数据
     *
     * @param pyid 主键
     * @return 影响行数
     */
    int deleteByPrimaryKey(String pyid);

}