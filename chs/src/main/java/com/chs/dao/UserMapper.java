package com.chs.dao;

import com.chs.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

/**
 * (User)表数据库访问层
 *
 * @author mopeiwen
 * @since 2020-06-10 10:49:15
 */
 @Mapper
public interface UserMapper {

    /**
     * 通过用户ID查询用户详细信息
     *
     * @param uid 主键
     * @return 实例对象
     */
    User selectByPrimaryKey(String uid);
    
    User selectByEmail(String email);
    
    User selectByPhone(String phone);
    
    /**
     * 通过账号来实现用户登录
     * @param account
     * @param password
     * @return
     */
    User selectByAccount(@Param(value = "user") User user);

    /**
     *查询所有用户列表（分页）
     * @param user 
     *
     * @param user 实例对象
     * @return 对象列表
     */
    List<User> selectAll();

    /**
     * 新增用户数据
     *
     * @param user 实例对象
     * @return 影响行数
     */
    int insertSelective(User user);

    /**
     * 修改用户数据
     *
     * @param user 实例对象
     * @return 影响行数
     */
    int updateByPrimaryKeySelective(User user);

    /**
     * 通过主键(用户id)删除用户数据
     *
     * @param uid 主键
     * @return 影响行数
     */
    int deleteByPrimaryKey(String uid);
    
    /**
     *批量删除用户
     * @param uids
     */
    int  batchdeleteUser(ArrayList<String> list);
    
    /**
     * 得到用户总数
     * @return
     */
    int getUserCount();
    
    /**
     * 模糊查询用户
     */
    List<User> findUserByList(String ss);

}