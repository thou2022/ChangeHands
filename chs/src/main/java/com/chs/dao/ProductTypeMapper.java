package com.chs.dao;

import com.chs.model.ProductType;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * (ProductType)表数据库访问层
 *
 * @author mopeiwen
 * @since 2020-06-10 10:49:02
 */
 @Mapper
public interface ProductTypeMapper {

    /**
     * 通过商品类型ID查询单条数据
     *
     * @param ptid 主键
     * @return 实例对象
     */
    ProductType selectByPrimaryKey(String ptid);
    

    /**
     * 查询所有商品类型
     *
     * @param productType 实例对象
     * @return 对象列表
     */
    List<ProductType> selectAll(ProductType productType);
    
    List<ProductType> selectAll();

    /**
     * 新增商品分类数据
     *
     * @param productType 实例对象
     * @return 影响行数
     */
    int insertSelective(ProductType productType);

    /**
     * 修改商品分类数据
     *
     * @param productType 实例对象
     * @return 影响行数
     */
    int updateByPrimaryKeySelective(ProductType productType);

    /**
     * 通过主键（商品类型id）删除商品分类数据
     *
     * @param ptid 主键
     * @return 影响行数
     */
    int deleteByPrimaryKey(String ptid);

}