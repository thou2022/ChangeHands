package com.chs.dao;

import com.chs.model.Notice;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;
import java.util.List;

/**
 * (Notice)表数据库访问层
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:11
 */
 @Mapper
public interface NoticeMapper {

    /**
     * 管理员通过公告ID查询单条数据
     *
     * @param nid 主键
     * @return 实例对象
     */
    Notice selectByPrimaryKey(String nid);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param notice 实例对象
     * @return 对象列表
     */
    List<Notice> selectAll();
    
    /**
     * 通过size查询
     * @param size
     * @return
     */
    List<Notice> selectAllBySize(int size);

    /**
     * 管理员新增公告数据
     *
     * @param notice 实例对象
     * @return 影响行数
     */
    int insertSelective(Notice notice);

    /**
     * 管理员修改数据
     *
     * @param notice 实例对象
     * @return 影响行数
     */
    int updateByPrimaryKeySelective(Notice notice);

    /**
     * 通过主键（订单id）删除数据
     *
     * @param nid 主键
     * @return 影响行数
     */
    int deleteByPrimaryKey(String nid);
    
    /**
     *批量删除公告
     * @param nids
     */
    int  batchdeleteNotice(ArrayList<String> list);

}