package com.chs.dao;

import com.chs.model.LoginHistory;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;
import java.util.List;

/**
 * (LoginHistory)表数据库访问层
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:04
 */
 @Mapper
public interface LoginHistoryMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param lhid 主键
     * @return 实例对象
     */
    LoginHistory selectByPrimaryKey(String lhid);

    /**
     * 查询所有登录记录（分页）
     *
     * @param loginHistory 实例对象
     * @return 对象列表
     */
    List<LoginHistory> selectAll();

    List<LoginHistory> selectAll(LoginHistory loginHistory);
    
    /**
     * 新增数据
     *
     * @param loginHistory 实例对象
     * @return 影响行数
     */
    int insertSelective(LoginHistory loginHistory);

    /**
     * 修改数据
     *
     * @param loginHistory 实例对象
     * @return 影响行数
     */
    int updateByPrimaryKeySelective(LoginHistory loginHistory);

    /**
     * 通过登录记录ID删除登录记录
     *
     * @param lhid 主键
     * @return 影响行数
     */
    int deleteByPrimaryKey(String lhid);
    
    /**
     *批量删除用户登录记录
     * @param uids
     */
    int  batchdeleteLoginHistory(ArrayList<String> list);
    
    
    /**
     * 模糊查询用户登录记录
     */
    List<LoginHistory> findLoginHistoryByList(String gh);

}