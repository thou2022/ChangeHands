package com.chs.dao;

import com.chs.model.SystemInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * (SystemInfo)表数据库访问层
 *
 * @author mopeiwen
 * @since 2020-06-10 10:49:10
 */
@Mapper
public interface SystemInfoMapper {

	/**
	 * 获得数据
	 * 
	 * @return 实例对象
	 */
	SystemInfo getSystemInfo();

	/**
	 * 修改数据
	 *
	 * @param systemInfo 实例对象
	 * @return 修改状态
	 */
	boolean updateSystemInfo(SystemInfo systemInfo);

}