package com.chs.dao;

import com.chs.model.ProductHistory;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * (ProductHistory)表数据库访问层
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:53
 */
 @Mapper
public interface ProductHistoryMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param phid 主键
     * @return 实例对象
     */
    ProductHistory selectByPrimaryKey(String phid);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param productHistory 实例对象
     * @return 对象列表
     */
    List<ProductHistory> selectAll(ProductHistory productHistory);

    /**
     * 新增数据
     *
     * @param productHistory 实例对象
     * @return 影响行数
     */
    int insertSelective(ProductHistory productHistory);

    /**
     * 修改数据
     *
     * @param productHistory 实例对象
     * @return 影响行数
     */
    int updateByPrimaryKeySelective(ProductHistory productHistory);

    /**
     * 通过主键删除数据
     *
     * @param phid 主键
     * @return 影响行数
     */
    int deleteByPrimaryKey(String phid);

}