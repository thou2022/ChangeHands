package com.chs.service;

import com.chs.model.Plate;
import java.util.List;

/**
 * (Plate)表服务接口
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:21
 */
public interface PlateService {

    /**
     * 通过ID查询单条数据
     *
     * @param pid 主键
     * @return 实例对象
     */
    Plate queryById(String pid);
    
    /**
     * 通过Plate对象查询集合
     *
     * @param plate 实例对象
     * @return 实例对象
     */
    List<Plate> selectAll();

    /**
     * 管理员新增板块数据
     * @param plate 实例对象
     * @return 实例对象
     */
    boolean insert(Plate plate);

    /**
     * 修改数据
     *
     * @param plate 实例对象
     * @return 实例对象
     */
    boolean update(Plate plate);

    /**
     * 通过主键删除数据
     *
     * @param pid 主键
     * @return 是否成功
     */
    boolean deleteById(String pid);

}