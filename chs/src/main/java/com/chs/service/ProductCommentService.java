package com.chs.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

import com.chs.model.ProductComment;
import com.chs.model.dto.CommentDto;
import com.github.pagehelper.PageInfo;

/**
 * (ProductComment)表服务接口
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:49
 */
public interface ProductCommentService {

	/**
	 * 通过ID查询单条数据
	 *
	 * @param pcid 主键
	 * @return 实例对象
	 */
	ProductComment queryById(String pcid);

	/**
	 * 通过ProductComment对象查询集合
	 *
	 * @param productComment 实例对象
	 * @return 实例对象
	 */
	PageInfo<ProductComment> selectAll(int pageNum, int pageSize);

	PageInfo<ProductComment> selectAll(int pageNum, int pageSize, String pid, Integer type, String pcid);

	PageInfo<ProductComment> selectMyAll(int pageNum, int pageSize);

	/**
	 * 新增数据
	 *
	 * @param productComment 实例对象
	 * @return 实例对象
	 */
	boolean insert(ProductComment productComment);

	boolean insertComments(CommentDto commentDto);

	/**
	 * 单个添加图片
	 * 
	 * @param file
	 * @param request
	 * @param response
	 * @return
	 */
	String insert(MultipartFile file, HttpServletRequest request, HttpServletResponse response);

	/**
	 * 修改数据
	 *
	 * @param productComment 实例对象
	 * @return 实例对象
	 */
	boolean update(ProductComment productComment);

	/**
	 * 通过主键删除数据
	 *
	 * @param pcid 主键
	 * @return 是否成功
	 */
	boolean deleteById(String pcid);

	/**
	 * 批量删除评论
	 * 
	 * @param pcids
	 */
	boolean batchdeleteComment(String pcid);

}