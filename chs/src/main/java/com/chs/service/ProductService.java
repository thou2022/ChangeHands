package com.chs.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

import com.chs.model.Product;
import com.github.pagehelper.PageInfo;

/**
 * (Product)表服务接口
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:38
 */
public interface ProductService {

	/**
	 * 通过商品ID查询单条商品数据
	 *
	 * @param pid 主键
	 * @return 实例对象
	 */
	Product queryById(String pid);

	Product queryByIdForHistory(String pid);

	/**
	 * 通过Product对象查询商品集合
	 *
	 * @param product 实例对象
	 * @return 实例对象
	 */
	PageInfo<Product> selectAll(int pageNum, int pageSize);

	PageInfo<Product> selectAll(int pageNum, int pageSize, String uid);

	PageInfo<Product> selectAllByType(int pageNum, int pageSize, String ptid);

	PageInfo<Product> selectAllForUser(int pageNum, int pageSize);

	/**
	 * 新增商品数据
	 *
	 * @param product 实例对象
	 * @return 实例对象
	 */
	boolean insert(Product product);

	boolean insertForUid(Product product);

	/**
	 * 单存图片
	 * 
	 * @param file
	 * @param request
	 * @param response
	 * @return
	 */
	String insertFile(MultipartFile file, HttpServletRequest request, HttpServletResponse response);

	boolean insert(MultipartFile[] file, Product product, HttpServletRequest request, HttpServletResponse response);

	/**
	 * 管理员修改数据
	 *
	 * @param product 实例对象
	 * @return 实例对象
	 */
	boolean update(Product product);

	boolean updateProFile(MultipartFile[] file, Product product, HttpServletRequest request,
			HttpServletResponse response);

	/**
	 * 通过主键（商品id）删除数据
	 *
	 * @param pid 主键
	 * @return 是否成功
	 */
	boolean deleteById(String pid);

	/**
	 * 批量删除商品
	 * 
	 * @param uids
	 */
	boolean batchdeleteProduct(String pid);

	/**
	 * 管理员审核商品
	 * 
	 * @param pid
	 * @return
	 */
	boolean examineProduct(Product product);

}