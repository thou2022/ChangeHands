package com.chs.service;

import com.chs.model.LoginHistory;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * (LoginHistory)表服务接口
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:04
 */
public interface LoginHistoryService {

    /**
     * 通过ID查询单条数据
     *
     * @param lhid 主键
     * @return 实例对象
     */
    LoginHistory queryById(String lhid);
    
    /**
     * 通过LoginHistory对象查询集合
     *
     * @param loginHistory 实例对象
     * @return 实例对象
     */
     PageInfo<LoginHistory> selectAll(int pageNum,int pageSize);

    /**
     * 新增数据
     *
     * @param loginHistory 实例对象
     * @return 实例对象
     */
     boolean insert(LoginHistory loginHistory);

    /**
     * 修改数据
     *
     * @param loginHistory 实例对象
     * @return 实例对象
     */
     boolean update(LoginHistory loginHistory);

    /**
     * 通过主键删除数据
     *
     * @param lhid 主键
     * @return 是否成功
     */
    boolean deleteById(String lhid);
    
    /**
	 * 批量删除用户登录记录
	 * 
	 * @param uids
	 */
	boolean batchdeleteLoginHistory(String lhid);

	List<LoginHistory> selectAll(LoginHistory loginHistory);

}