package com.chs.service;

import com.chs.model.SystemInfo;

/**
 * (SystemInfo)表服务接口
 *
 * @author mopeiwen
 * @since 2020-06-10 10:49:10
 */
public interface SystemInfoService {

	/**
	 * 获得数据
	 * 
	 * @return 实例对象
	 */
	SystemInfo getSystemInfo();

	/**
	 * 修改数据
	 *
	 * @param systemInfo 实例对象
	 * @return 修改状态
	 */
	boolean updateSystemInfo(SystemInfo systemInfo);

}