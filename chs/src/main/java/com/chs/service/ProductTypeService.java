package com.chs.service;

import com.chs.model.ProductType;
import java.util.List;

/**
 * (ProductType)表服务接口
 *
 * @author mopeiwen
 * @since 2020-06-10 10:49:02
 */
public interface ProductTypeService {

    /**
     * 通过ID查询单条数据
     *
     * @param ptid 主键
     * @return 实例对象
     */
    ProductType queryById(String ptid);
    
    /**
     * 通过ProductType对象查询所有商品类型
     *
     * @param productType 实例对象
     * @return 实例对象
     */
    List<ProductType> selectAll(ProductType productType);
    
    List<ProductType> selectAll();

    /**
     * 新增商品类型数据
     *
     * @param productType 实例对象
     * @return 实例对象
     */
    boolean insert(ProductType productType);

    /**
     * 修改商品类型数据
     *
     * @param productType 实例对象
     * @return 实例对象
     */
    boolean update(ProductType productType);

    /**
     * 通过主键（商品类型id）删除商品分类数据
     *
     * @param ptid 主键
     * @return 是否成功
     */
    boolean deleteById(String ptid);

}