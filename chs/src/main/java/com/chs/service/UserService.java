package com.chs.service;

import java.io.UnsupportedEncodingException;

import com.chs.model.User;
import com.chs.model.dto.UserDto;
import com.github.pagehelper.PageInfo;

/**
 * (User)表服务接口
 *
 * @author mopeiwen
 * @since 2020-06-10 10:49:15
 */
public interface UserService {

	/**
	 * 通过用户ID查询用户详细信息
	 *
	 * @param uid 主键
	 * @return 实例对象
	 */
	User queryById(String uid);

	/**
	 * 通过User对象查询集合
	 *
	 * @param user 实例对象
	 * @return 实例对象
	 */
	PageInfo<User> selectAll(int pageNum, int pageSize);

	/**
	 * 通过账号来实现用户登录
	 * 
	 * @param account
	 * @param password
	 * @return
	 */
	UserDto selectByAccount(String account, String password) throws UnsupportedEncodingException;

	/**
	 * 获得验证码
	 * 
	 * @return
	 */
	boolean getCode(String username);

	/**
	 * 新增用户数据
	 *
	 * @param user 实例对象
	 * @return 实例对象
	 */
	boolean insert(UserDto user);

	boolean insert(User user);

	/**
	 * 修改用户数据信息
	 *
	 * @param user 实例对象
	 * @return 实例对象
	 */
	boolean update(User user);

	boolean updatePassword(UserDto user);

	boolean updatePhoneOrEmail(UserDto user);

	/**
	 * 通过主键(用户id)删除用户数据
	 *
	 * @param uid 主键
	 * @return 是否成功
	 */
	boolean deleteById(String uid);

	/**
	 * 批量删除用户
	 * 
	 * @param uids
	 */
	boolean batchdeleteUser(String uid);

}