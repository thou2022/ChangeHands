package com.chs.service;

import com.chs.model.Order;
import com.chs.model.dto.OrderDto;
import com.github.pagehelper.PageInfo;

/**
 * (Order)表服务接口
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:17
 */
public interface OrderService {

	/**
	 * 获得订单信息
	 * @param pid 单个商品下单
	 * @param scids 购物车下单 多个商品
	 * @return
	 */
	OrderDto queryById(String pid_count,String scids);
	
	/**
	 * 确认订单
	 * @param orderDto
	 * @return
	 */
	String insertOrder(OrderDto orderDto);
	
	/**
	 * 支付成功修改订单记录
	 *
	 * @param oid 主键
	 * @return 实例对象
	 */
	boolean orderPay(String oid);
	
    /**
     * 通过订单ID查询单条订单数据
     *
     * @param oid 主键
     * @return 实例对象
     */
    Order queryById(String oid);
    
    /**
     * 查询所有订单列表
     *
     * @param order 实例对象
     * @return 实例对象
     */
    PageInfo<Order> selectAll(int pageNum,int pageSize, int flag);
    
    PageInfo<Order> selectAll(int pageNum,int pageSize);
    
    PageInfo<Order> selectAllMySold(int pageNum,int pageSize);

    /**
     * 新增数据
     *
     * @param order 实例对象
     * @return 实例对象
     */
    boolean insert(Order order);

    /**
     * 修改数据
     *
     * @param order 实例对象
     * @return 实例对象
     */
    boolean update(Order order);

    /**
     * 通过主键（订单id）删除数据
     *
     * @param oid 主键
     * @return 是否成功
     */
    boolean deleteById(String oid);
    
    /**
     *批量删除订单
     * @param uids
     */
    boolean batchdeleteOrder(String oid);
    
    /**
     * 填写物流号
     * @param oid
     * @param logisticsNo
     * @return
     */
    boolean writeLogNo(String oid,String logisticsNo);
    
    /**
     * 确认收货
     * @param oid
     * @return
     */
    boolean okReceipt(String oid);

}