package com.chs.service;

import com.chs.model.ProductHistory;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * (ProductHistory)表服务接口
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:53
 */
public interface ProductHistoryService {

    /**
     * 通过ID查询单条数据
     *
     * @param phid 主键
     * @return 实例对象
     */
    ProductHistory queryById(String phid);
    
    /**
     * 通过ProductHistory对象查询集合
     *
     * @param productHistory 实例对象
     * @return 实例对象
     */
    List<ProductHistory> selectAll(ProductHistory productHistory);
    
    PageInfo<ProductHistory> selectAll(int pageNum,int pageSize,String ptid);

    /**
     * 新增数据
     *
     * @param productHistory 实例对象
     * @return 实例对象
     */
    boolean insert(ProductHistory productHistory);

    /**
     * 修改数据
     *
     * @param productHistory 实例对象
     * @return 实例对象
     */
    boolean update(ProductHistory productHistory);

    /**
     * 通过主键删除数据
     *
     * @param phid 主键
     * @return 是否成功
     */
    boolean deleteById(String phid);
    
    boolean deleteAll(String uid);

}