package com.chs.service;

import com.chs.model.Admin;
import com.chs.model.LoginHistory;
import com.chs.model.Order;
import com.chs.model.Product;
import com.chs.model.ProductComment;
import com.chs.model.User;
import com.chs.model.dto.IoDto;

import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * (Admin)表服务接口
 *
 * @author mopeiwen
 * @since 2020-06-10 10:47:19
 */
public interface AdminService {

	/**
	 * 通过ID查询单条数据
	 *
	 * @param aid 主键
	 * @return 实例对象
	 */
	Admin queryById(String aid);

	/**
	 * 通过账号来实现管理员登录
	 * 
	 * @param account
	 * @param password
	 * @return
	 */
	Admin selectByAccount(String account, String password) throws UnsupportedEncodingException;

	/**
	 * 通过Admin对象查询集合
	 *
	 * @param admin 实例对象
	 * @return 实例对象
	 */
	List<Admin> selectAll(Admin admin);

	Admin selectById();

	/**
	 * 新增数据
	 *
	 * @param admin 实例对象
	 * @return 实例对象
	 */
	boolean insert(Admin admin);

	/**
	 * 修改数据
	 *
	 * @param admin 实例对象
	 * @return 实例对象
	 */
	boolean update(Admin admin);

	/**
	 * 通过主键删除数据
	 *
	 * @param aid 主键
	 * @return 是否成功
	 */
	boolean deleteById(String aid);

	/**
	 * 获取用户总数
	 * 
	 * @return
	 */
	IoDto getIoDto();

	/**
	 * 模糊查询用户
	 */
	List<User> findUserByList(String ss);
	
	/**
     * 模糊查询商品
     */
    List<Product> findProductByList(String pp);
    
    /**
     * 模糊查询订单
     */
    List<Order> findOrderByList(String oo);

    /**
     * 模糊查询商品评论
     */
    List<ProductComment> findCommentByList(String cc);
    

    /**
     * 模糊查询用户登录记录
     */
    List<LoginHistory> findLoginHistoryByList(String gh);
}