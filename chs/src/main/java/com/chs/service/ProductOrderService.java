package com.chs.service;

import com.chs.model.ProductOrder;
import java.util.List;

/**
 * (ProductOrder)表服务接口
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:57
 */
public interface ProductOrderService {

    /**
     * 通过ID查询单条数据
     *
     * @param poid 主键
     * @return 实例对象
     */
    ProductOrder queryById(String poid);
    
    /**
     * 通过ProductOrder对象查询集合
     *
     * @param productOrder 实例对象
     * @return 实例对象
     */
    List<ProductOrder> selectAll(ProductOrder productOrder);

    /**
     * 新增数据
     *
     * @param productOrder 实例对象
     * @return 实例对象
     */
    boolean insert(ProductOrder productOrder);

    /**
     * 修改数据
     *
     * @param productOrder 实例对象
     * @return 实例对象
     */
    boolean update(ProductOrder productOrder);

    /**
     * 通过主键删除数据
     *
     * @param poid 主键
     * @return 是否成功
     */
    boolean deleteById(String poid);

}