package com.chs.service;

import com.chs.model.ShoppingCart;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * (ShoppingCart)表服务接口
 *
 * @author mopeiwen
 * @since 2020-06-10 10:49:06
 */
public interface ShoppingCartService {

    /**
     * 通过ID查询单条数据
     *
     * @param scid 主键
     * @return 实例对象
     */
    ShoppingCart queryById(String scid);
    
    /**
     * 通过ShoppingCart对象查询集合
     *
     * @param shoppingCart 实例对象
     * @return 实例对象
     */
    List<ShoppingCart> selectAll(ShoppingCart shoppingCart);
    
    PageInfo<ShoppingCart> selectAll(int pageNum,int pageSize,String uid);

    /**
     * 新增数据
     *
     * @param shoppingCart 实例对象
     * @return 实例对象
     */
    boolean insert(ShoppingCart shoppingCart);

    /**
     * 修改数据
     *
     * @param shoppingCart 实例对象
     * @return 实例对象
     */
    boolean update(ShoppingCart shoppingCart);

    /**
     * 通过主键删除数据
     *
     * @param scid 主键
     * @return 是否成功
     */
    boolean deleteById(String scid);
    
    boolean deleteByIds(String scid);

}