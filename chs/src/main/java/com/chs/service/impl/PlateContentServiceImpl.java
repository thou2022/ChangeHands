package com.chs.service.impl;

import com.chs.model.PlateContent;
import com.chs.dao.PlateContentMapper;
import com.chs.service.PlateContentService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * (PlateContent)表服务实现类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:27
 */
@Service
public class PlateContentServiceImpl implements PlateContentService {
    @Resource
    private PlateContentMapper plateContentMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param pcid 主键
     * @return 实例对象
     */
    @Override
    public PlateContent queryById(String pcid) {
        return this.plateContentMapper.selectByPrimaryKey(pcid);
    }
    
    /**
     * 通过PlateContent对象查询集合
     *
     * @param plateContent 实例对象
     * @return 实例对象
     */
    @Override
    public PageInfo<PlateContent> selectAll(int pageNum,int pageSize) {
    	// pageNum代表页码值，pageSize代表每页条数
		PageHelper.startPage(pageNum, pageSize);
		// 使用PageInfo来封装查询的数据
		PageInfo<PlateContent> pageInfo = new PageInfo<PlateContent>(plateContentMapper.selectAll());
		return pageInfo;
    }

    /**
     * 新增数据
     *
     * @param plateContent 实例对象
     * @return 实例对象
     */
    @Override
    public boolean insert(PlateContent plateContent) {
        return this.plateContentMapper.insertSelective(plateContent)>0;
    }

    /**
     * 修改数据
     *
     * @param plateContent 实例对象
     * @return 实例对象
     */
    @Override
    public boolean update(PlateContent plateContent) {
        return this.plateContentMapper.updateByPrimaryKeySelective(plateContent)>0;
    }

    /**
     * 通过主键删除数据
     *
     * @param pcid 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String pcid) {
        return this.plateContentMapper.deleteByPrimaryKey(pcid) > 0;
    }
}