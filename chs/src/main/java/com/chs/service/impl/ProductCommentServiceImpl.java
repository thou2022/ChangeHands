package com.chs.service.impl;

import com.chs.model.Order;
import com.chs.model.Product;
import com.chs.model.ProductComment;
import com.chs.model.dto.CommentDto;
import com.chs.dao.OrderMapper;
import com.chs.dao.ProductCommentMapper;
import com.chs.service.ProductCommentService;
import com.chs.util.FileUtil;
import com.chs.util.SourceUtil;
import com.chs.util.TXTUtil;
import com.chs.util.TokenUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.ArrayList;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * (ProductComment)表服务实现类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:49
 */
@Service
public class ProductCommentServiceImpl implements ProductCommentService {
	@Resource
	private ProductCommentMapper productCommentMapper;
	@Resource
	private OrderMapper orderMapper;
	@Autowired
	private HttpServletRequest request;

	/**
	 * 通过ID查询单条数据
	 *
	 * @param pcid 主键
	 * @return 实例对象
	 */
	@Override
	public ProductComment queryById(String pcid) {
		return this.productCommentMapper.selectByPrimaryKey(pcid);
	}

	/**
	 * 通过ProductComment对象查询集合
	 *
	 * @param productComment 实例对象
	 * @return 实例对象
	 */
	@Override
	public PageInfo<ProductComment> selectAll(int pageNum, int pageSize) {
		// pageNum代表页码值，pageSize代表每页条数
		PageHelper.startPage(pageNum, pageSize);
		// 使用PageInfo来封装查询的数据
		PageInfo<ProductComment> pageInfo = new PageInfo<ProductComment>(productCommentMapper.selectAll());

		return pageInfo;
	}

	@Override
	public PageInfo<ProductComment> selectMyAll(int pageNum, int pageSize) {
		// pageNum代表页码值，pageSize代表每页条数
		PageHelper.startPage(pageNum, pageSize);
		
		ProductComment productComment = new ProductComment();
		productComment.setUid(TokenUtil.getUserId((String) request.getHeader("Authorization")));

		// 使用PageInfo来封装查询的数据
		PageInfo<ProductComment> pageInfo = new PageInfo<ProductComment>(productCommentMapper.selectMyAll(productComment));

		// 防止指针指向相同的对象
		for (int i = 0; i < pageInfo.getList().size(); i++) {
			Product product = pageInfo.getList().get(i).getProduct();
			if (product.getPicture().contains("http")) {
				continue;
			}

			pageInfo.getList().get(i).getProduct().setPicture(TXTUtil.splitAndSplice(
					pageInfo.getList().get(i).getProduct().getPicture(), ",", SourceUtil.getPictureUrl()));
			System.out.println(pageInfo.getList().get(i).getProduct().getPicture());
		}
		
		return pageInfo;
	}
	

	/*
	 * 用户查看评论列表
	 */
	@Override
	public PageInfo<ProductComment> selectAll(int pageNum,int pageSize,String pid,Integer type,String pcid){
		// pageNum代表页码值，pageSize代表每页条数
		PageHelper.startPage(pageNum, pageSize);

		ProductComment productComment = new ProductComment();
		productComment.setPid(pid);
		productComment.setType(type);
		if (pcid!=null&&!pcid.equals("")) {
			productComment.setPcid(pcid);
		}

		// 使用PageInfo来封装查询的数据
		PageInfo<ProductComment> pageInfo = new PageInfo<ProductComment>(productCommentMapper.selectAll(productComment));
		
		return pageInfo;
	}
	
	/**
	 * 新增数据
	 *
	 * @param productComment 实例对象
	 * @return 实例对象
	 */
	@Override
	public boolean insert(ProductComment productComment) {
		productComment.setUid(TokenUtil.getUserId((String) request.getHeader("Authorization")));
		productComment.setAddtime(DateUtil.now());
		
		//回复评论
		productComment.setType(1);
		
		return this.productCommentMapper.insertSelective(productComment)>0;
	}

	@Override
	public boolean insertComments(CommentDto commentDto) {

		for (ProductComment productComment : commentDto.getComments()) {
			productComment.setUid(TokenUtil.getUserId((String) request.getHeader("Authorization")));
			productComment.setAddtime(DateUtil.now());
			
			//商品评论
			productComment.setType(0);
			this.productCommentMapper.insertSelective(productComment);
		}
		// 更新订单状态
		Order order = new Order();
		order.setOid(commentDto.getOid());

		order.setOrderflag(4);
		return this.orderMapper.updateByPrimaryKeySelective(order) > 0;
	}
	
	// 单存图片
	@Override
	public String insert(MultipartFile file, HttpServletRequest request, HttpServletResponse response) {
		String asName = null;
		try {
			asName = "picture_" + DateUtil.format(DateUtil.date(), "yyyyMMddHHmmss");
		} catch (Exception e) {
			e.printStackTrace();
			asName = "picture_" + IdUtil.simpleUUID();
		}

		File file0 = FileUtil.upFilebySpring(file, request, response, asName, "source/picture");
		return "picture/" + file0.getName();
	}

	/**
	 * 修改数据
	 *
	 * @param productComment 实例对象
	 * @return 实例对象
	 */
	@Override
	public boolean update(ProductComment productComment) {
		return this.productCommentMapper.updateByPrimaryKeySelective(productComment)>0;
	}

	/**
	 * 通过主键删除数据
	 *
	 * @param pcid 主键
	 * @return 是否成功
	 */
	@Override
	public boolean deleteById(String pcid) {
		return this.productCommentMapper.deleteByPrimaryKey(pcid)>0;
	}

	/**
	 * 批量删除评论
	 */
	@Override
	public boolean batchdeleteComment(String pcid) {
		ArrayList<String> pcidList = new ArrayList<String>();
		String[] strs = pcid.split(",");
		for (String str : strs) {
			pcidList.add(str);
		}
		return productCommentMapper.batchdeleteComment(pcidList)>0;
	}

}