package com.chs.service.impl;

import com.chs.model.Plate;
import com.chs.dao.PlateMapper;
import com.chs.service.PlateService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Plate)表服务实现类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:21
 */
@Service
public class PlateServiceImpl implements PlateService {
    @Resource
    private PlateMapper plateMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param pid 主键
     * @return 实例对象
     */
    @Override
    public Plate queryById(String pid) {
        return this.plateMapper.selectByPrimaryKey(pid);
    }
    
    /**
     * 通过Plate对象查询集合
     *
     * @param plate 实例对象
     * @return 实例对象
     */
    @Override
    public List<Plate> selectAll() {
        return this.plateMapper.selectAll();
    }

    /**
     * 管理员新增板块数据
     * @param plate 实例对象
     * @return 实例对象
     */
    @Override
    public boolean insert(Plate plate) {
        return this.plateMapper.insertSelective(plate)>0;
    }

    /**
     * 修改数据
     *
     * @param plate 实例对象
     * @return 实例对象
     */
    @Override
    public boolean update(Plate plate) {
        return this.plateMapper.updateByPrimaryKeySelective(plate)>0;
    }

    /**
     * 通过主键删除数据
     *
     * @param pid 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String pid) {
        return this.plateMapper.deleteByPrimaryKey(pid) > 0;
    }
}