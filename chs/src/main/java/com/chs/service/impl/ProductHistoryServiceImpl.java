package com.chs.service.impl;

import com.chs.model.Product;
import com.chs.model.ProductHistory;
import com.chs.dao.ProductHistoryMapper;
import com.chs.service.ProductHistoryService;
import com.chs.util.SourceUtil;
import com.chs.util.TXTUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.util.List;

/**
 * (ProductHistory)表服务实现类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:53
 */
@Service
public class ProductHistoryServiceImpl implements ProductHistoryService {
    @Resource
    private ProductHistoryMapper productHistoryMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param phid 主键
     * @return 实例对象
     */
    @Override
    public ProductHistory queryById(String phid) {
    	ProductHistory productHistory = this.productHistoryMapper.selectByPrimaryKey(phid);

    	productHistory.getProduct().setPicture(TXTUtil.splitAndSplice(productHistory.getProduct().getPicture(), ",", SourceUtil.getPictureUrl()));
		System.out.println(productHistory.getProduct().getPicture());
    	
        return productHistory;
    }
    
    /**
     * 通过ProductHistory对象查询集合
     *
     * @param productHistory 实例对象
     * @return 实例对象
     */
    @Override
    public List<ProductHistory> selectAll(ProductHistory productHistory) {
    	List<ProductHistory> list = this.productHistoryMapper.selectAll(productHistory);
    	
    	//防止指针指向相同的对象
        Product product = new Product(); 
		for (int i = 0; i < list.size(); i++) {
			product = list.get(i).getProduct();
			if (product.getPicture().contains("http")) {
				continue;
			}
			
			list.get(i).getProduct().setPicture(TXTUtil.splitAndSplice(list.get(i).getProduct().getPicture(), ",", SourceUtil.getPictureUrl()));
			System.out.println(list.get(i).getProduct().getPicture());
		}
    	
        return this.productHistoryMapper.selectAll(productHistory);
    }

    @Override
    public PageInfo<ProductHistory> selectAll(int pageNum,int pageSize,String uid){
    	//pageNum代表页码值，pageSize代表每页条数
    	PageHelper.startPage(pageNum,pageSize);
    	
    	ProductHistory productHistory = new ProductHistory();
    	productHistory.setUid(uid);
    	
    	//使用PageInfo来封装查询的数据
        PageInfo<ProductHistory> pageInfo = new PageInfo<ProductHistory>(productHistoryMapper.selectAll(productHistory));
        
        //防止指针指向相同的对象
        Product product = new Product(); 
		for (int i = 0; i < pageInfo.getList().size(); i++) {
			product = pageInfo.getList().get(i).getProduct();
			if (product.getPicture().contains("http")) {
				continue;
			}
			
			pageInfo.getList().get(i).getProduct().setPicture(TXTUtil.splitAndSplice(pageInfo.getList().get(i).getProduct().getPicture(), ",", SourceUtil.getPictureUrl()));
			System.out.println(pageInfo.getList().get(i).getProduct().getPicture());
		}
        
		return pageInfo;
    	
    }
    
    /**
     * 新增数据
     *
     * @param productHistory 实例对象
     * @return 实例对象
     */
    @Override
    public boolean insert(ProductHistory productHistory) {
        return this.productHistoryMapper.insertSelective(productHistory)>0;
    }

    /**
     * 修改数据
     *
     * @param productHistory 实例对象
     * @return 实例对象
     */
    @Override
    public boolean update(ProductHistory productHistory) {
        return this.productHistoryMapper.updateByPrimaryKeySelective(productHistory)>0;
    }

    /**
     * 通过主键删除数据
     *
     * @param phid 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String phid) {
        return this.productHistoryMapper.deleteByPrimaryKey(phid) > 0;
    }
    
    @Override
    public boolean deleteAll(String uid) {
    	ProductHistory productHistory = new ProductHistory();
    	productHistory.setUid(uid);
    	
    	List<ProductHistory> productHistories = this.productHistoryMapper.selectAll(productHistory);

    	for (ProductHistory ph : productHistories) {
    		this.productHistoryMapper.deleteByPrimaryKey(ph.getPhid());
		}

        return true;
    }
}