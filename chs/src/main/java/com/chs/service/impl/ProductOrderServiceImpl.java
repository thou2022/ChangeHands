package com.chs.service.impl;

import com.chs.model.ProductOrder;
import com.chs.dao.ProductOrderMapper;
import com.chs.service.ProductOrderService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (ProductOrder)表服务实现类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:57
 */
@Service
public class ProductOrderServiceImpl implements ProductOrderService {
    @Resource
    private ProductOrderMapper productOrderMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param poid 主键
     * @return 实例对象
     */
    @Override
    public ProductOrder queryById(String poid) {
        return this.productOrderMapper.selectByPrimaryKey(poid);
    }
    
    /**
     * 通过ProductOrder对象查询集合
     *
     * @param productOrder 实例对象
     * @return 实例对象
     */
    @Override
    public List<ProductOrder> selectAll(ProductOrder productOrder) {
        return this.productOrderMapper.selectAll(productOrder);
    }

    /**
     * 新增数据
     *
     * @param productOrder 实例对象
     * @return 实例对象
     */
    @Override
    public boolean insert(ProductOrder productOrder) {
        return this.productOrderMapper.insertSelective(productOrder)>0;
    }

    /**
     * 修改数据
     *
     * @param productOrder 实例对象
     * @return 实例对象
     */
    @Override
    public boolean update(ProductOrder productOrder) {
        return this.productOrderMapper.updateByPrimaryKeySelective(productOrder)>0;
    }

    /**
     * 通过主键删除数据
     *
     * @param poid 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String poid) {
        return this.productOrderMapper.deleteByPrimaryKey(poid) > 0;
    }
}