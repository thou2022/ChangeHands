package com.chs.service.impl;

import com.chs.model.LoginHistory;
import com.chs.dao.LoginHistoryMapper;
import com.chs.service.LoginHistoryService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.util.ArrayList;
import java.util.List;

/**
 * (LoginHistory)表服务实现类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:04
 */
@Service
public class LoginHistoryServiceImpl implements LoginHistoryService {
    @Resource
    private LoginHistoryMapper loginHistoryMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param lhid 主键
     * @return 实例对象
     */
    @Override
    public LoginHistory queryById(String lhid) {
        return this.loginHistoryMapper.selectByPrimaryKey(lhid);
    }
    
    /**
     * 通过LoginHistory对象查询集合
     *
     * @param loginHistory 实例对象
     * @return 实例对象
     */
    @Override
    public PageInfo<LoginHistory> selectAll(int pageNum,int pageSize) {
    	//pageNum代表页码值，pageSize代表每页条数
    	PageHelper.startPage(pageNum,pageSize);
    	//使用PageInfo来封装查询的数据
        PageInfo<LoginHistory> pageInfo = new PageInfo<LoginHistory>(loginHistoryMapper.selectAll());
        
        return pageInfo;
    }

    /**
     * 新增数据
     *
     * @param loginHistory 实例对象
     * @return 实例对象
     */
    @Override
    public boolean insert(LoginHistory loginHistory) {
        return this.loginHistoryMapper.insertSelective(loginHistory)>0;
    }

    /**
     * 修改数据
     *
     * @param loginHistory 实例对象
     * @return 实例对象
     */
    @Override
    public boolean update(LoginHistory loginHistory) {
        return this.loginHistoryMapper.updateByPrimaryKeySelective(loginHistory)>0;
    }

    /**
     * 通过主键删除数据
     *
     * @param lhid 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String lhid) {
        return this.loginHistoryMapper.deleteByPrimaryKey(lhid)>0;
    }

	@Override
	public List<LoginHistory> selectAll(LoginHistory loginHistory) {
		return this.loginHistoryMapper.selectAll(loginHistory);
	}

	/**
	 * 批量删除用户登录记录
	 */
	@Override
	public boolean batchdeleteLoginHistory(String lhid) {
		ArrayList<String> lhidList = new ArrayList<String>();
		String[] strs = lhid.split(",");
		for (String str : strs) {
			lhidList.add(str);
		}
		return loginHistoryMapper.batchdeleteLoginHistory(lhidList)>0;
		
	}
}