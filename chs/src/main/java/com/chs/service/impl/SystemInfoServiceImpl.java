package com.chs.service.impl;

import com.chs.model.SystemInfo;
import com.chs.dao.SystemInfoMapper;
import com.chs.service.SystemInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * (SystemInfo)表服务实现类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:49:10
 */
@Service
public class SystemInfoServiceImpl implements SystemInfoService {
    @Resource
    private SystemInfoMapper systemInfoMapper;

	/**
	 * 获得数据
	 * 
	 * @return 实例对象
	 */
	@Override
	public SystemInfo getSystemInfo() {
		return systemInfoMapper.getSystemInfo();
	}

	/**
	 * 修改数据
	 *
	 * @param systemInfo 实例对象
	 * @return 修改状态
	 */
	@Override
	public boolean updateSystemInfo(SystemInfo systemInfo) {
		return systemInfoMapper.updateSystemInfo(systemInfo);
	}
    
}