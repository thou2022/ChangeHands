package com.chs.service.impl;

import com.chs.model.ProductType;
import com.chs.dao.ProductTypeMapper;
import com.chs.service.ProductTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (ProductType)表服务实现类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:49:02
 */
@Service
public class ProductTypeServiceImpl implements ProductTypeService {
    @Resource
    private ProductTypeMapper productTypeMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param ptid 主键
     * @return 实例对象
     */
    @Override
    public ProductType queryById(String ptid) {
    	 System.out.println(ptid);
        return this.productTypeMapper.selectByPrimaryKey(ptid);
       
    }
    
    /**
     * 通过ProductType对象查询所有商品类型
     *
     * @param productType 实例对象
     * @return 实例对象
     */
    @Override
    public List<ProductType> selectAll(ProductType productType) {
        return this.productTypeMapper.selectAll(productType);
    }
    
    @Override
    public List<ProductType> selectAll() {
        return this.productTypeMapper.selectAll();
    }

    /**
     * 新增商品类型数据
     *
     * @param productType 实例对象
     * @return 实例对象
     */
    @Override
    public boolean insert(ProductType productType) {
        return this.productTypeMapper.insertSelective(productType)>0;
    }

    /**
     * 修改商品类型数据
     *
     * @param productType 实例对象
     * @return 实例对象
     */
    @Override
    public boolean update(ProductType productType) {
       return this.productTypeMapper.updateByPrimaryKeySelective(productType)>0;
    }

    /**
     * 通过主键（商品类型id）删除商品分类数据
     *
     * @param ptid 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String ptid) {
        return this.productTypeMapper.deleteByPrimaryKey(ptid) > 0;
    }
}