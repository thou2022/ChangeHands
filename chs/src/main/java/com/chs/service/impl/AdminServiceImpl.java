package com.chs.service.impl;

import com.chs.model.Admin;
import com.chs.model.LoginHistory;
import com.chs.model.Order;
import com.chs.model.Product;
import com.chs.model.ProductComment;
import com.chs.model.User;
import com.chs.model.dto.IoDto;
import com.chs.dao.AdminMapper;
import com.chs.dao.LoginHistoryMapper;
import com.chs.dao.OrderMapper;
import com.chs.dao.ProductCommentMapper;
import com.chs.dao.ProductMapper;
import com.chs.dao.SystemInfoMapper;
import com.chs.dao.UserMapper;
import com.chs.exception.ServiceException;
import com.chs.service.AdminService;
import com.chs.util.TokenUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * (Admin)表服务实现类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:47:20
 */
@Service
@Transactional
public class AdminServiceImpl implements AdminService {
	@Resource
	private AdminMapper adminMapper;
	@Resource
	private UserMapper userMapper;
	@Resource
	private ProductMapper productMapper;
	@Resource
	private OrderMapper orderMapper;
	@Resource
	private SystemInfoMapper systemInfoMapper;
	@Resource
	private ProductCommentMapper productCommentMapper;
	@Resource
	private LoginHistoryMapper loginHistoryMapper;
	@Autowired
	private HttpServletRequest request;
	@Autowired
	private HttpServletResponse response;

	/**
	 * 通过ID查询单条数据
	 *
	 * @param aid 主键
	 * @return 实例对象
	 */
	@Override
	public Admin queryById(String aid) {
		return this.adminMapper.selectByPrimaryKey(aid);
	}

	/**
	 * 通过Admin对象查询集合
	 *
	 * @param admin 实例对象
	 * @return 实例对象
	 */
	@Override
	public List<Admin> selectAll(Admin admin) {
		return this.adminMapper.selectAll(admin);
	}

	@Override
	public Admin selectById() {
		Admin admin = this.adminMapper.selectByPrimaryKey(TokenUtil.getUserId(request.getHeader("Authorization")));
		
		if (admin==null) {
			return null;
		}
		
		admin.setPassword(null);
		return admin;
	}

	/**
	 * 新增数据
	 *
	 * @param admin 实例对象
	 * @return 实例对象
	 */
	@Override
	public boolean insert(Admin admin) {
		return this.adminMapper.insertSelective(admin)>0;
	}

	/**
	 * 修改数据
	 *
	 * @param admin 实例对象
	 * @return 实例对象
	 */
	@Override
	public boolean update(Admin admin) {
		String aid = TokenUtil.getUserId(request.getHeader("Authorization"));
		admin.setAid(aid);
		
		return this.adminMapper.updateByPrimaryKeySelective(admin)>0;
	}

	/**
	 * 通过主键删除数据
	 *
	 * @param aid 主键
	 * @return 是否成功
	 */
	@Override
	public boolean deleteById(String aid) {
		return this.adminMapper.deleteByPrimaryKey(aid) > 0;
	}

	/**
	 * 通过账号来实现管理员登录
	 * 
	 * @param account
	 * @param password
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@Override
	public Admin selectByAccount(String account, String password) throws UnsupportedEncodingException {
		Admin admin = adminMapper.selectByAccount(account, password);

		if (admin != null) {
			admin.setPassword(null);
		} else {
			throw new ServiceException(-1, "管理员密码错误");
		}

		response.setHeader("Authorization", TokenUtil.sign(admin.getAid()));

		return admin;
	}

	@Override
	public IoDto getIoDto() {
		Integer volume = orderMapper.getOrderCount();
		Integer userCount = userMapper.getUserCount();
		Integer productCount = productMapper.getProductCount();
		Integer visits = systemInfoMapper.getSystemInfo().getVisits();

		IoDto iDto = new IoDto(volume, userCount, productCount, visits);

		return iDto;
	}

	/**
	 * 管理员模糊查询用户
	 */
	@Override
	public List<User> findUserByList(String ss) {
		return userMapper.findUserByList(ss);
	}

	/**
	 * 管理员模糊查询商品
	 */
	@Override
	public List<Product> findProductByList(String pp) {
		return productMapper.findProductByList(pp);
	}

	/**
	 * 模糊查询订单
	 */
	@Override
	public List<Order> findOrderByList(String oo) {
		return orderMapper.findOrderByList(oo);
	}

	/**
	 * 模糊查询商品评论
	 */
	@Override
	public List<ProductComment> findCommentByList(String cc) {
		return productCommentMapper.findCommentByList(cc);
	}

	@Override
	public List<LoginHistory> findLoginHistoryByList(String gh) {
		return loginHistoryMapper.findLoginHistoryByList(gh);
	}

	
}
