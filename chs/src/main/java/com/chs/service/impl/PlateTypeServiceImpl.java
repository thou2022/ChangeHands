package com.chs.service.impl;

import com.chs.model.PlateType;
import com.chs.dao.PlateTypeMapper;
import com.chs.service.PlateTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (PlateType)表服务实现类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:32
 */
@Service
public class PlateTypeServiceImpl implements PlateTypeService {
    @Resource
    private PlateTypeMapper plateTypeMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param pyid 主键
     * @return 实例对象
     */
    @Override
    public PlateType queryById(String pyid) {
        return this.plateTypeMapper.selectByPrimaryKey(pyid);
    }
    
    /**
     * 通过PlateType对象查询集合
     *
     * @param plateType 实例对象
     * @return 实例对象
     */
    @Override
    public List<PlateType> selectAll() {
        return this.plateTypeMapper.selectAll();
    }

    /**
     * 新增板块类型数据
     *
     * @param plateType 实例对象
     * @return 实例对象
     */
    @Override
    public boolean insert(PlateType plateType) {
        return this.plateTypeMapper.insertSelective(plateType)>0;
    }

    /**
     * 修改数据
     *
     * @param plateType 实例对象
     * @return 实例对象
     */
    @Override
    public boolean update(PlateType plateType) {
        return  this.plateTypeMapper.updateByPrimaryKeySelective(plateType)>0;
    }

    /**
     * 通过主键删除数据
     *
     * @param pyid 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String pyid) {
        return this.plateTypeMapper.deleteByPrimaryKey(pyid) > 0;
    }
}