package com.chs.service.impl;

import com.chs.model.Notice;
import com.chs.dao.NoticeMapper;
import com.chs.service.NoticeService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.hutool.core.date.DateUtil;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

/**
 * (Notice)表服务实现类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:11
 */
@Service
public class NoticeServiceImpl implements NoticeService {
	@Resource
	private NoticeMapper noticeMapper;

	/**
	 * 管理员通过公告ID查询单条数据
	 *
	 * @param nid 主键
	 * @return 实例对象
	 */
	@Override
	public Notice queryById(String nid) {
		return this.noticeMapper.selectByPrimaryKey(nid);
	}

	/**
	 * pageNum和pageSize查询集合
	 *
	 * @param pageNum
	 * @param pageSize
	 * @return 实例对象
	 */
	@Override
	public PageInfo<Notice> selectAll(int pageNum, int pageSize) {
		// pageNum代表页码值，pageSize代表每页条数
		PageHelper.startPage(pageNum, pageSize);
		// 使用PageInfo来封装查询的数据
		PageInfo<Notice> pageInfo = new PageInfo<Notice>(noticeMapper.selectAll());
		return pageInfo;
	}
	
	/**
	 * 通过Notice对象查询集合
	 *
	 * @param pageNum
	 * @param pageSize
	 * @return 实例对象
	 */
	@Override
	public List<Notice> selectAllBySize(int size) {
		return noticeMapper.selectAllBySize(size);
	}

	/**
	 * 管理员新增公告数据
	 *
	 * @param notice 实例对象
	 * @return 实例对象
	 */
	@Override
	public boolean insert(Notice notice) {
		notice.setIssuetime(DateUtil.now());
		return this.noticeMapper.insertSelective(notice)>0;
	}

	/**
	 * 管理员修改数据
	 *
	 * @param notice 实例对象
	 * @return 实例对象
	 */
	@Override
	public boolean update(Notice notice) {
		return this.noticeMapper.updateByPrimaryKeySelective(notice)>0;
	}

	/**
	 * 通过主键（订单id）删除数据
	 *
	 * @param nid 主键
	 * @return 是否成功
	 */
	@Override
	public boolean deleteById(String nid) {
		return this.noticeMapper.deleteByPrimaryKey(nid)>0;
	}

	/**
	 * 管理员批量删除公告
	 */
	@Override
	public boolean batchdeleteNotice(String nid) {
			ArrayList<String> nidList = new ArrayList<String>();
			String[] strs = nid.split(",");
			for (String str : strs) {
				nidList.add(str);
			}
			return noticeMapper.batchdeleteNotice(nidList)>0;
		}
}