package com.chs.service.impl;

import com.chs.model.Product;
import com.chs.model.ShoppingCart;
import com.chs.dao.ShoppingCartMapper;
import com.chs.exception.ServiceException;
import com.chs.service.ShoppingCartService;
import com.chs.util.SourceUtil;
import com.chs.util.TXTUtil;
import com.chs.util.TokenUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import java.util.List;

/**
 * (ShoppingCart)表服务实现类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:49:06
 */
@Service
@Transactional
public class ShoppingCartServiceImpl implements ShoppingCartService {
    @Resource
    private ShoppingCartMapper shoppingCartMapper;
    @Autowired
    private HttpServletRequest request;
    
    /**
     * 通过ID查询单条数据
     *
     * @param scid 主键
     * @return 实例对象
     */
    @Override
    public ShoppingCart queryById(String scid) {
        return this.shoppingCartMapper.selectByPrimaryKey(scid);
    }
    
    /**
     * 通过ShoppingCart对象查询集合
     *
     * @param shoppingCart 实例对象
     * @return 实例对象
     */
    @Override
    public List<ShoppingCart> selectAll(ShoppingCart shoppingCart) {
        return this.shoppingCartMapper.selectAll(shoppingCart);
    }

	@Override
	public PageInfo<ShoppingCart> selectAll(int pageNum, int pageSize, String uid) {
		//pageNum代表页码值，pageSize代表每页条数
    	PageHelper.startPage(pageNum,pageSize);
    	
    	ShoppingCart shop = new ShoppingCart();
    	shop.setUid(uid);
    	
    	//使用PageInfo来封装查询的数据
        PageInfo<ShoppingCart> pageInfo = new PageInfo<ShoppingCart>(shoppingCartMapper.selectAll(shop));
        
        //防止指针指向相同的对象
        Product product = new Product(); 
		for (int i = 0; i < pageInfo.getList().size(); i++) {
			product = pageInfo.getList().get(i).getProduct();
			if (product.getPicture().contains("http")) {
				continue;
			}

			product.setPicture(TXTUtil.splitAndSplice(product.getPicture(), ",", SourceUtil.getPictureUrl()));
			pageInfo.getList().get(i).setProduct(product);
			System.out.println(product.getPicture());
		}
        
        return pageInfo;
	}
	
    /**
     * 新增数据
     *
     * @param shoppingCart 实例对象
     * @return 实例对象
     */
    @Override
    public boolean insert(ShoppingCart shoppingCart) {
    	shoppingCart.setUid(TokenUtil.getUserId((String)request.getHeader("Authorization")));
    	ShoppingCart shop = this.shoppingCartMapper.selectByPid(shoppingCart.getPid());
    	if (shop!=null) { //数据库已存在购物项，即更新
    		shoppingCart.setScid(shop.getScid());
    		
    		if (shoppingCart.getType()!=null&&shoppingCart.getType()==1) {
    			
    			if (shoppingCart.getCount()<=0) {//如果数量少于0
					throw new ServiceException(-1, "商品数量不能少于0");
				}
    			
    			shoppingCart.setCount(shoppingCart.getCount());
			}else {
				shoppingCart.setCount(shop.getCount()+shoppingCart.getCount());
			}
    		
			this.shoppingCartMapper.updateByPrimaryKeySelective(shoppingCart);
		}else {
			shoppingCart.setEnable(true);
			this.shoppingCartMapper.insertSelective(shoppingCart);
		}

        return true;
    }

    /**
     * 修改数据
     *
     * @param shoppingCart 实例对象
     * @return 实例对象
     */
    @Override
    public boolean update(ShoppingCart shoppingCart) {
        return this.shoppingCartMapper.updateByPrimaryKeySelective(shoppingCart)>0;
    }

    /**
     * 通过主键删除数据
     *
     * @param scid 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String scid) {
        return this.shoppingCartMapper.deleteByPrimaryKey(scid) > 0;
    }
    
    @Override
    public boolean deleteByIds(String scid) {
    	String[] ids = TXTUtil.valueToStr(scid, ",");
    	
    	for (String id : ids) {
    		this.shoppingCartMapper.deleteByPrimaryKey(id);
		}
    	
        return true;
    }

}