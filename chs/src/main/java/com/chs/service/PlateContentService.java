package com.chs.service;

import com.chs.model.PlateContent;
import com.github.pagehelper.PageInfo;

/**
 * (PlateContent)表服务接口
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:27
 */
public interface PlateContentService {

    /**
     * 通过ID查询单条数据
     *
     * @param pcid 主键
     * @return 实例对象
     */
    PlateContent queryById(String pcid);
    
    /**
     * 通过PlateContent对象查询集合
     *
     * @param plateContent 实例对象
     * @return 实例对象
     */
    PageInfo<PlateContent> selectAll(int pageNum,int pageSize);

    /**
     * 新增数据
     *
     * @param plateContent 实例对象
     * @return 实例对象
     */
    boolean insert(PlateContent plateContent);

    /**
     * 修改数据
     *
     * @param plateContent 实例对象
     * @return 实例对象
     */
    boolean update(PlateContent plateContent);

    /**
     * 通过主键删除数据
     *
     * @param pcid 主键
     * @return 是否成功
     */
    boolean deleteById(String pcid);

}