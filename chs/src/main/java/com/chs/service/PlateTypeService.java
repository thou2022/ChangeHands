package com.chs.service;

import com.chs.model.PlateType;
import java.util.List;

/**
 * (PlateType)表服务接口
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:32
 */
public interface PlateTypeService {

    /**
     * 通过ID查询单条数据
     *
     * @param pyid 主键
     * @return 实例对象
     */
    PlateType queryById(String pyid);
    
    /**
     * 通过PlateType对象查询集合
     *
     * @param plateType 实例对象
     * @return 实例对象
     */
    List<PlateType> selectAll();

    /**
     * 新增板块类型数据
     *
     * @param plateType 实例对象
     * @return 实例对象
     */
    boolean insert(PlateType plateType);

    /**
     * 修改数据
     *
     * @param plateType 实例对象
     * @return 实例对象
     */
    boolean update(PlateType plateType);

    /**
     * 通过主键删除数据
     *
     * @param pyid 主键
     * @return 是否成功
     */
    boolean deleteById(String pyid);

}