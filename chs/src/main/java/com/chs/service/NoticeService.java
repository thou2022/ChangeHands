package com.chs.service;

import com.chs.model.Notice;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * (Notice)表服务接口
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:11
 */
public interface NoticeService {

    /**
     * 管理员通过公告ID查询单条数据
     *
     * @param nid 主键
     * @return 实例对象
     */
    Notice queryById(String nid);
    
	/**
	 * pageNum和pageSize查询集合
	 *
	 * @param pageNum
	 * @param pageSize
	 * @return 实例对象
	 */
    PageInfo<Notice> selectAll(int pageNum,int pageSize);

    /**
     * 管理员新增公告数据
     *
     * @param notice 实例对象
     * @return 实例对象
     */
    boolean insert(Notice notice);

    /**
     * 管理员修改数据
     *
     * @param notice 实例对象
     * @return 实例对象
     */
    boolean update(Notice notice);

    /**
     * 通过主键（订单id）删除数据
     *
     * @param nid 主键
     * @return 是否成功
     */
    boolean deleteById(String nid);
    
    /**
     *批量删除公告
     * @param nids
     */
    boolean batchdeleteNotice(String nid);

	/**
	 * size查询集合
	 *
	 * @param size
	 * @return 实例对象
	 */
	List<Notice> selectAllBySize(int size);

}