package com.chs;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.chs.dao")
public class ChsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChsApplication.class, args);
	}

}
