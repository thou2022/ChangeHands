package com.chs.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

/**
 * (ShoppingCart)实体类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:49:06
 */
public class ShoppingCart implements Serializable {
    private static final long serialVersionUID = 103996488438591123L;
    //购物项ID
    private String scid;
    //用户ID 指买家用户信息
    //@NotBlank(message = "用户ID不能为空")
    private String uid;
    //商品ID
    //@NotBlank(message = "商品ID不能为空")
    private String pid;
    private Product product;
    //商品数量
    @NotNull(message = "商品数量不能为空")
    private Integer count;
    private Integer type;
    
    private Boolean enable;

    public String getScid() {
        return scid;
    }

    public void setScid(String scid) {
        this.scid = scid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
    
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Boolean getEnable() {
		return enable;
	}

	public void setEnable(Boolean enable) {
		this.enable = enable;
	}

	@Override
	public String toString() {
		return "ShoppingCart [scid=" + scid + ", uid=" + uid + ", pid=" + pid + ", product=" + product + ", count="
				+ count + ", type=" + type + ", enable=" + enable + "]";
	}


}