package com.chs.model;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

/**
 * (PlateContent)实体类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:27
 */
public class PlateContent implements Serializable {
    private static final long serialVersionUID = 703273524225989220L;
    //板块内容ID
    private String pcid;
    //板块ID
    private String plid;
    //商品ID
    private String ptid;
    private Product product;
    //板块内容显示名
    @NotBlank(message = "板块内容显示名不能为空")
    private String showname;
    //板块内容缩略图
    private String thumbnail;
    //板块内容优先级
    private Integer priority;


    public String getPcid() {
        return pcid;
    }

    public void setPcid(String pcid) {
        this.pcid = pcid;
    }

    public String getPlid() {
        return plid;
    }

    public void setPlid(String plid) {
        this.plid = plid;
    }

    public String getPtid() {
        return ptid;
    }

    public void setPtid(String ptid) {
        this.ptid = ptid;
    }

    public String getShowname() {
        return showname;
    }

    public void setShowname(String showname) {
        this.showname = showname;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public String toString() {
		return "PlateContent [pcid=" + pcid + ", plid=" + plid + ", ptid=" + ptid + ", product=" + product
				+ ", showname=" + showname + ", thumbnail=" + thumbnail + ", priority=" + priority + "]";
	}
    
}