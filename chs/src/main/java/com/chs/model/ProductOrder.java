package com.chs.model;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

/**
 * (ProductOrder)实体类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:57
 */
public class ProductOrder implements Serializable {
    private static final long serialVersionUID = -89117994405755326L;
    //商品订单关系表ID
    private String poid;
    //商品ID
    @NotBlank(message = "商品不能为空")
    private String pid;
    private Product product;
    //订单ID
    @NotBlank(message = "订单不能为空")
    private String oid;
    //商品数量
    @NotBlank(message = "商品数量不能为空")
    private Integer count;


    public String getPoid() {
        return poid;
    }

    public void setPoid(String poid) {
        this.poid = poid;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

	@Override
	public String toString() {
		return "ProductOrder [poid=" + poid + ", pid=" + pid + ", oid=" + oid + ", count=" + count + "]";
	}

	public ProductOrder(String poid, @NotBlank(message = "商品不能为空") String pid, @NotBlank(message = "订单不能为空") String oid,
			@NotBlank(message = "商品数量不能为空") Integer count) {
		super();
		this.poid = poid;
		this.pid = pid;
		this.oid = oid;
		this.count = count;
	}

	public ProductOrder() {
		super();
	}
    
}