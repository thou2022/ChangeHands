package com.chs.model;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
/**
 * (Admin)实体类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:47:18
 */
public class Admin implements Serializable {
    private static final long serialVersionUID = 795430071985657754L;
    //管理员ID
   // @NotBlank(message = "管理员ID不能为空")
    private String aid;
    //管理员账号
    @NotBlank(message = "管理员账号不能为空")
    private String account;
    //管理员名字
    //@NotBlank(message = "管理员名字不能为空")
    private String name;
    //管理员密码
    @NotBlank(message = "管理员密码不能为空")
    @Pattern(regexp = "^(?![a-zA-Z]+$)(?![0-9]+$)[A-Za-z0-9]{8,18}$", message = "密码必须为8-18为数字与字母组合")
    private String password;
    //管理员权限
    private Integer root;


    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getRoot() {
        return root;
    }

    public void setRoot(Integer root) {
        this.root = root;
    }

}