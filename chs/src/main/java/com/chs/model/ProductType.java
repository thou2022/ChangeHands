package com.chs.model;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

/**
 * (ProductType)实体类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:49:02
 */
public class ProductType implements Serializable {
    private static final long serialVersionUID = 231282907607353920L;
    //商品类型ID
    private String ptid;
    //商品类型名
    @NotBlank(message = "商品类型名不能为空")
    private String name;
    //商品类型优先级
    private Integer priority;
    //商品类型启用状态
    private Boolean enableflag;

    public String getPtid() {
        return ptid;
    }

    public void setPtid(String ptid) {
        this.ptid = ptid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Boolean getEnableflag() {
        return enableflag;
    }

    public void setEnableflag(Boolean enableflag) {
        this.enableflag = enableflag;
    }

	public ProductType(String ptid) {
		super();
		this.ptid = ptid;
	}

	public ProductType() {
		super();
	}

	@Override
	public String toString() {
		return "ProductType [ptid=" + ptid + ", name=" + name + ", priority=" + priority + ", enableflag=" + enableflag
				+ "]";
	}
    
}