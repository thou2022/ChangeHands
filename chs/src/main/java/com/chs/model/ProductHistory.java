package com.chs.model;

import java.io.Serializable;

/**
 * (ProductHistory)实体类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:53
 */
public class ProductHistory implements Serializable {
    private static final long serialVersionUID = -84235657372923043L;
    //商品浏览记录ID
    private String phid;
    //用户ID
    private String uid;
    //商品ID
    private String pid;
    private Product product;
    //浏览时间
    private String browsetime;

    public String getPhid() {
        return phid;
    }

    public void setPhid(String phid) {
        this.phid = phid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getBrowsetime() {
        return browsetime;
    }

    public void setBrowsetime(String browsetime) {
        this.browsetime = browsetime;
    }

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public ProductHistory(String phid, String uid, String pid, Product product, String browsetime) {
		super();
		this.phid = phid;
		this.uid = uid;
		this.pid = pid;
		this.product = product;
		this.browsetime = browsetime;
	}

	public ProductHistory(String phid, String uid, String pid, String browsetime) {
		super();
		this.phid = phid;
		this.uid = uid;
		this.pid = pid;
		this.browsetime = browsetime;
	}

	@Override
	public String toString() {
		return "ProductHistory [phid=" + phid + ", uid=" + uid + ", pid=" + pid + ", product=" + product
				+ ", browsetime=" + browsetime + "]";
	}

	public ProductHistory() {
		super();
	}
    
}