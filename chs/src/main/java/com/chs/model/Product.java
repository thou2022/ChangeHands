package com.chs.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Range;

/**
 * (Product)实体类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:38
 */
public class Product implements Serializable {
	private static final long serialVersionUID = -67408482701260555L;
	// 商品ID
	// @NotBlank(message = "商品不能为空")
	private String pid;
	// 发布人ID 卖家用户信息
	// @NotBlank(message = "发布人不能为空")
	private String uid;
	private User user;
	// 商品类型ID
	// @NotBlank(message = "商品类型不能为空")
	private String ptid;
	private ProductType productType;
	// 商品名称
	 @NotBlank(message = "商品名称不能为空")
	private String name;
	// 商品描述
	private String describe;
	// 商品图片
	 @NotBlank(message = "商品图片不能为空")
	private String picture;
	// 商品原价
	// @NotBlank(message = "商品原价不能为空")
	 @Range(min = 1,message = "商品原价最小不能为0")
	private BigDecimal originalprice;
	// 商品现价
	// @NotBlank(message = "商品现价不能为空")
	 @Range(min = 1,message = "商品现价最小不能为0")
	private BigDecimal price;
	// 商品运费
	private BigDecimal fare;
	// 商品数量
	 //@NotBlank(message = "商品数量不能为空")
	 @Range(min = 1,message = "商品数量最小不能为0")
	private Integer count;
	// 商品发布时间
	private String releasetime;
	// 商品上架状态
	private Boolean putonflag;
	// 商品审核状态
	private Integer examineflag;
	// 商品是否删除
	private Boolean delflag;

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

    public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public BigDecimal getOriginalprice() {
		return originalprice;
	}

	public void setOriginalprice(BigDecimal originalprice) {
		this.originalprice = originalprice;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getFare() {
		return fare;
	}

	public void setFare(BigDecimal fare) {
		this.fare = fare;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getReleasetime() {
		return releasetime;
	}

	public void setReleasetime(String releasetime) {
		this.releasetime = releasetime;
	}

	public Boolean getPutonflag() {
		return putonflag;
	}

	public void setPutonflag(Boolean putonflag) {
		this.putonflag = putonflag;
	}

	public Integer getExamineflag() {
		return examineflag;
	}

	public void setExamineflag(Integer examineflag) {
		this.examineflag = examineflag;
	}

	public Boolean getDelflag() {
		return delflag;
	}

	public void setDelflag(Boolean delflag) {
		this.delflag = delflag;
	}

	public String getPtid() {
		return ptid;
	}

	public void setPtid(String ptid) {
		this.ptid = ptid;
	}

	@Override
	public String toString() {
		return "Product [pid=" + pid + ", uid=" + uid + ", user=" + user + ", ptid=" + ptid + ", productType="
				+ productType + ", name=" + name + ", describe=" + describe + ", picture=" + picture
				+ ", originalprice=" + originalprice + ", price=" + price + ", fare=" + fare + ", count=" + count
				+ ", releasetime=" + releasetime + ", putonflag=" + putonflag + ", examineflag=" + examineflag
				+ ", delflag=" + delflag + "]";
	}

	public Product() {
		super();
	}
}
