package com.chs.model.dto;

import java.math.BigDecimal;

//商品的Dto
public class ProductDto {
	// 商品ID
	private String pid;
	// 商品名
	private String name;
	// 商品现价
	private BigDecimal price;
	// 商品运费
	private BigDecimal fare;
	// 商品购买的数量
	private Integer count;
	// 商品图片
	private String picture;
	
	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getFare() {
		return fare;
	}

	public void setFare(BigDecimal fare) {
		this.fare = fare;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}
	
	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	@Override
	public String toString() {
		return "ProductDto [pid=" + pid + ", name=" + name + ", price=" + price + ", fare=" + fare + ", count=" + count
				+ ", picture=" + picture + "]";
	}

	public ProductDto(String pid, String name, BigDecimal price, BigDecimal fare, Integer count, String picture) {
		super();
		this.pid = pid;
		this.name = name;
		this.price = price;
		this.fare = fare;
		this.count = count;
		this.picture = picture;
	}

	public ProductDto() {
		super();
	}
	
}
