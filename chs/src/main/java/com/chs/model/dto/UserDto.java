package com.chs.model.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import com.chs.model.User;

public class UserDto {
	// 用户
	@Valid 
	private User user;
	// 登录账号
	@NotBlank(message = "账号不能为空")
	private String username;
	// 验证码
	//@NotBlank(message = "验证码不能为空")
	private String code;
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "UserDto [user=" + user + ", username=" + username + ", code=" + code + "]";
	}

	public UserDto(User user, String username, String code) {
		super();
		this.user = user;
		this.username = username;
		this.code = code;
	}

	public UserDto() {
		super();
	}

}
