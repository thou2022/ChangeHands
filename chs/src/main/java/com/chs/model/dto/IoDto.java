package com.chs.model.dto;

public class IoDto {
	Integer volume;// 交易量
	Integer userCount;// 用户量
	Integer productCount;// 商品量
	Integer visits;// 访问量

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	public int getUserCount() {
		return userCount;
	}

	public void setUserCount(int userCount) {
		this.userCount = userCount;
	}

	public int getProductCount() {
		return productCount;
	}

	public void setProductCount(int productCount) {
		this.productCount = productCount;
	}

	public int getVisits() {
		return visits;
	}

	public void setVisits(int visits) {
		this.visits = visits;
	}

	@Override
	public String toString() {
		return "IoDto [volume=" + volume + ", userCount=" + userCount + ", productCount=" + productCount + ", visits="
				+ visits + "]";
	}

	public IoDto(int volume, int userCount, int productCount, int visits) {
		super();
		this.volume = volume;
		this.userCount = userCount;
		this.productCount = productCount;
		this.visits = visits;
	}

	public IoDto() {
		super();
	}
}
