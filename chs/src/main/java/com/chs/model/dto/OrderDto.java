package com.chs.model.dto;

import java.math.BigDecimal;
import java.util.List;

import com.chs.model.User;

public class OrderDto {

	// 商品的Dto
	List<ProductDto> products;
	// 购买人
	User user;
	// 收货方式
	int orderType;
	// 运费
	BigDecimal fare;

	public List<ProductDto> getProducts() {
		return products;
	}

	public void setProducts(List<ProductDto> products) {
		this.products = products;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getOrderType() {
		return orderType;
	}

	public void setOrderType(int orderType) {
		this.orderType = orderType;
	}

	public BigDecimal getFare() {
		return fare;
	}

	public void setFare(BigDecimal fare) {
		this.fare = fare;
	}

	@Override
	public String toString() {
		return "OrderDto [products=" + products + ", user=" + user + ", orderType=" + orderType + ", fare=" + fare
				+ "]";
	}

}
