package com.chs.model.dto;

import java.util.List;

import com.chs.model.ProductComment;

public class CommentDto {
	//评论Dto
	List<ProductComment> comments;
	//订单ID
	String oid;
	
	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public List<ProductComment> getComments() {
		return comments;
	}

	public void setComments(List<ProductComment> comments) {
		this.comments = comments;
	}
	
}
