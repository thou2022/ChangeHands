package com.chs.model;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotBlank;

/**
 * (Plate)实体类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:21
 */
public class Plate implements Serializable {
    private static final long serialVersionUID = 615257355127438241L;
    //板块ID
    private String pid;
    //板块类型ID
    private String pyid;
    //板块名称
    @NotBlank(message = "板块名称不能为空")
    private String name;
    //板块启用状态
    private Boolean enableflag;
    //板块优先级
    private Integer priority;
    
    private List<PlateContent> plateContents;


    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getPyid() {
        return pyid;
    }

    public void setPyid(String pyid) {
        this.pyid = pyid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getEnableflag() {
        return enableflag;
    }

    public void setEnableflag(Boolean enableflag) {
        this.enableflag = enableflag;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

	public List<PlateContent> getPlateContents() {
		return plateContents;
	}

	public void setPlateContents(List<PlateContent> plateContents) {
		this.plateContents = plateContents;
	}

	@Override
	public String toString() {
		return "Plate [pid=" + pid + ", plateContents=" + plateContents + ", pyid=" + pyid + ", name=" + name
				+ ", enableflag=" + enableflag + ", priority=" + priority + "]";
	}
    

}