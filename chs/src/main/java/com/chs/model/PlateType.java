package com.chs.model;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

/**
 * (PlateType)实体类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:32
 */
public class PlateType implements Serializable {
    private static final long serialVersionUID = -43227873583511694L;
    //板块类型ID
    private String pyid;
    //类型名称
    @NotBlank(message = "板块类型名称不能为空")
    private String name;
    //板块模板路径
    private String path;
    //板块最大内容
    private Integer maxcontent;
    //板块优先级
    private Integer priority;
    //板块启用状态
    private Boolean enableflag;


    public String getPyid() {
        return pyid;
    }

    public void setPyid(String pyid) {
        this.pyid = pyid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getMaxcontent() {
        return maxcontent;
    }

    public void setMaxcontent(Integer maxcontent) {
        this.maxcontent = maxcontent;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Boolean getEnableflag() {
        return enableflag;
    }

    public void setEnableflag(Boolean enableflag) {
        this.enableflag = enableflag;
    }

}