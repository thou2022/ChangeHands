package com.chs.model;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

/**
 * (Notice)实体类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:11
 */
public class Notice implements Serializable {
    private static final long serialVersionUID = 819973594254993297L;
    //公告ID
   // @NotBlank(message = "公告不能为空")
    private String nid;
    //公告内容
    @NotBlank(message = "公告内容不能为空")
    private String content;
    //公告发布时间
    private String issuetime;
    //公告优先级
    @NotNull(message = "公告优先级不能为空")
    @Range(min = 0, max = 99, message = "公告优先级必须为0-99")
    private Integer priority;
    //公告启用状态
    private Boolean enableflag;
    //公告标题
    @NotBlank(message = "公告标题不能为空")
    private String title;

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getIssuetime() {
        return issuetime;
    }

    public void setIssuetime(String issuetime) {
        this.issuetime = issuetime;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Boolean getEnableflag() {
        return enableflag;
    }

    public void setEnableflag(Boolean enableflag) {
        this.enableflag = enableflag;
    }

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}