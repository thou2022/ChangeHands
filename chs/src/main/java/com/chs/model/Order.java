package com.chs.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.validation.constraints.NotBlank;

/**
 * (Order)实体类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:17
 */
public class Order implements Serializable {
    private static final long serialVersionUID = -75765425774127851L;
    //订单的ID
    private String oid;
    //用户ID
    private String uid;
    //收货名
    @NotBlank(message = "收货名不能为空")
    private String name;
    //收货地址
    @NotBlank(message = "收货地址不能为空")
    private String receiptaddress;
    //收货电话
    private String receiptPhone;
    //下单时间
    private String placetime;
    //订单付款时间
    private String paytime;
    //确认收货时间
    private String receipttime;
    //物流单号
    @NotBlank(message = "物流单号不能为空")
    private String logisticsno;
    //收货方式
    @NotBlank(message = "收货方式不能为空")
    private Integer receiptway;
    //订单状态
    private Integer orderflag;
    //订单的总价
    private BigDecimal totalprice;

    private List<ProductOrder> productOrders;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getReceiptaddress() {
        return receiptaddress;
    }

    public void setReceiptaddress(String receiptaddress) {
        this.receiptaddress = receiptaddress;
    }

    public String getPlacetime() {
        return placetime;
    }

    public void setPlacetime(String placetime) {
        this.placetime = placetime;
    }

    public String getPaytime() {
        return paytime;
    }

    public void setPaytime(String paytime) {
        this.paytime = paytime;
    }

    public String getReceipttime() {
        return receipttime;
    }

    public void setReceipttime(String receipttime) {
        this.receipttime = receipttime;
    }

    public String getLogisticsno() {
        return logisticsno;
    }

    public void setLogisticsno(String logisticsno) {
        this.logisticsno = logisticsno;
    }

    public Integer getReceiptway() {
        return receiptway;
    }

    public void setReceiptway(Integer receiptway) {
        this.receiptway = receiptway;
    }

    public Integer getOrderflag() {
        return orderflag;
    }

    public void setOrderflag(Integer orderflag) {
        this.orderflag = orderflag;
    }

    public BigDecimal getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(BigDecimal totalprice) {
        this.totalprice = totalprice;
    }

	public List<ProductOrder> getProductOrders() {
		return productOrders;
	}

	public void setProductOrders(List<ProductOrder> productOrders) {
		this.productOrders = productOrders;
	}

	public String getReceiptPhone() {
		return receiptPhone;
	}

	public void setReceiptPhone(String receiptPhone) {
		this.receiptPhone = receiptPhone;
	}

	public Order(String uid, @NotBlank(message = "收货名不能为空") String name,
			@NotBlank(message = "收货地址不能为空") String receiptaddress, String receiptPhone, String placetime,
			String paytime, String receipttime, @NotBlank(message = "物流单号不能为空") String logisticsno,
			@NotBlank(message = "收货方式不能为空") Integer receiptway, Integer orderflag, BigDecimal totalprice){
		super();
		this.uid = uid;
		this.name = name;
		this.receiptaddress = receiptaddress;
		this.receiptPhone = receiptPhone;
		this.placetime = placetime;
		this.paytime = paytime;
		this.receipttime = receipttime;
		this.logisticsno = logisticsno;
		this.receiptway = receiptway;
		this.orderflag = orderflag;
		this.totalprice = totalprice;
	}

	@Override
	public String toString() {
		return "Order [oid=" + oid + ", uid=" + uid + ", name=" + name + ", receiptaddress=" + receiptaddress
				+ ", receiptPhone=" + receiptPhone + ", placetime=" + placetime + ", paytime=" + paytime
				+ ", receipttime=" + receipttime + ", logisticsno=" + logisticsno + ", receiptway=" + receiptway
				+ ", orderflag=" + orderflag + ", totalprice=" + totalprice + ", productOrders=" + productOrders + "]";
	}

	public Order() {
		super();
	}

}