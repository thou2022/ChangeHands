package com.chs.model;

import java.io.Serializable;

/**
 * (SystemInfo)实体类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:49:10
 */
public class SystemInfo implements Serializable {
    private static final long serialVersionUID = -36929357228801431L;
    //访问量
    private Integer visits;
    //公告开关
    private Boolean noticeflag;
    //网站名称
    private String websitename;
    //网站描述
    private String websitedescribe;
    //网站关键词
    private String websitekeyword;


    public Integer getVisits() {
        return visits;
    }

    public void setVisits(Integer visits) {
        this.visits = visits;
    }

    public Boolean getNoticeflag() {
        return noticeflag;
    }

    public void setNoticeflag(Boolean noticeflag) {
        this.noticeflag = noticeflag;
    }

    public String getWebsitename() {
        return websitename;
    }

    public void setWebsitename(String websitename) {
        this.websitename = websitename;
    }

    public String getWebsitedescribe() {
        return websitedescribe;
    }

    public void setWebsitedescribe(String websitedescribe) {
        this.websitedescribe = websitedescribe;
    }

    public String getWebsitekeyword() {
        return websitekeyword;
    }

    public void setWebsitekeyword(String websitekeyword) {
        this.websitekeyword = websitekeyword;
    }

}