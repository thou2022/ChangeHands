package com.chs.model;

import java.io.Serializable;

/**
 * (LoginHistory)实体类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:04
 */
public class LoginHistory implements Serializable {
	private static final long serialVersionUID = 424164655927913567L;
	// 登录记录ID
	private String lhid;
	// 用户ID
	private String uid;
	private User user;

	// 登录IP
	private String ip;
	// 登录设备
	private String device;
	// 登录时间
	private String time;

	public LoginHistory(String uid, String ip, String device, String time) {
		super();
		this.uid = uid;
		this.ip = ip;
		this.device = device;
		this.time = time;
	}

	public LoginHistory() {
		super();
	}

	@Override
	public String toString() {
		return "LoginHistory [lhid=" + lhid + ", uid=" + uid + ", ip=" + ip + ", device=" + device + ", time=" + time
				+ "]";
	}

	public String getLhid() {
		return lhid;
	}

	public void setLhid(String lhid) {
		this.lhid = lhid;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

}