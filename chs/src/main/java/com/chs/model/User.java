package com.chs.model;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.Length;
/**
 * (User)实体类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:49:15
 */
public class User implements Serializable {
    private static final long serialVersionUID = 296351624279033902L;
    //用户ID
    private String uid;
    //微信openID
    private String openid;
    //QQopenID
    private String qqopenid;
    //电话
    @Pattern(regexp ="^((17[0-9])|(14[0-9])|(13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$",message = "手机号码格式不正确")
    private String phone;
    //邮箱
    @Email
    //@NotBlank(message = "邮箱不能为空")
    @Pattern(regexp = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$",message = "邮箱格式不正确")
    private String email;
    //昵称 
    @NotBlank(message = "昵称不能为空")
    @Length(min = 1, max = 8, message = "昵称长度必须为6-12位")
    private String name;
    //性别,'男'或'女'
    //@NotBlank(message = "性别不能为空")
    private String sex;
    //收货地址
    //@NotBlank(message = "收货地址不能为空")
    private String address;
    //注册时间
    private String registertime;
    //登录密码
	/*
	 * @NotNull(message = "密码不能为空")
	 * 
	 * @Length(min = 6, max = 18, message = "密码长度必须为6-18位")
	 * 
	 * @Pattern(regexp = "^[a-zA-Z0-9]{6,20}$",message = "密码格式不正确")
	 */
    @NotBlank(message = "密码不能为空")
    @Pattern(regexp = "^(?![a-zA-Z]+$)(?![0-9]+$)[A-Za-z0-9]{8,18}$", message = "密码必须为8-18为数字与字母组合")
    private String password;


    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getQqopenid() {
        return qqopenid;
    }

    public void setQqopenid(String qqopenid) {
        this.qqopenid = qqopenid;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRegistertime() {
        return registertime;
    }

    public void setRegistertime(String registertime) {
        this.registertime = registertime;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

	@Override
	public String toString() {
		return "User [uid=" + uid + ", openid=" + openid + ", qqopenid=" + qqopenid + ", phone=" + phone + ", email="
				+ email + ", name=" + name + ", sex=" + sex + ", address=" + address + ", registertime=" + registertime
				+ ", password=" + password + "]";
	}

}