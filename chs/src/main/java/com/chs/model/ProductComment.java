package com.chs.model;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

/**
 * (ProductComment)实体类
 *
 * @author mopeiwen
 * @since 2020-06-10 10:48:49
 */
public class ProductComment implements Serializable {
    private static final long serialVersionUID = -55140934527281389L;
    //商品评论记录ID
    private String pcid;
    
    //用户ID
    private String uid;
    private User user;
    
    //被评论人ID
    private String quid;
    private User quer;
    
    //商品ID
	private String pid;
	private Product product;
	
    //内容
    @NotBlank(message = "评论内容不能为空")
    private String context;
    //评论时间
    private String addtime;
    //评论图片
    private String picture;
    //评分
    //@NotBlank(message = "商品评分不能为空")
    private Integer score;
    //评论类型
    private Integer type;
    
	public String getPcid() {
		return pcid;
	}
	public void setPcid(String pcid) {
		this.pcid = pcid;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getQuid() {
		return quid;
	}
	public void setQuid(String quid) {
		this.quid = quid;
	}
	public User getQuer() {
		return quer;
	}
	public void setQuer(User quer) {
		this.quer = quer;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public String getContext() {
		return context;
	}
	public void setContext(String context) {
		this.context = context;
	}
	public String getAddtime() {
		return addtime;
	}
	public void setAddtime(String addtime) {
		this.addtime = addtime;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	
	@Override
	public String toString() {
		return "ProductComment [pcid=" + pcid + ", uid=" + uid + ", user=" + user + ", quid=" + quid + ", quer=" + quer
				+ ", pid=" + pid + ", product=" + product + ", context=" + context + ", addtime=" + addtime
				+ ", picture=" + picture + ", score=" + score + ", type=" + type + "]";
	}
	
}