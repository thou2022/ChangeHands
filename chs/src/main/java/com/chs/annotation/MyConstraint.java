package com.chs.annotation;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.chs.annotation.impl.MyConstraintValidator;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.ElementType;

// @Target —— 表示这个注解可以作用在什么地方，例如作用在方法上，或作用在某个字段上。
// @Retention —— 被它所注解的注解保留多久，runtime表示不仅被保存到class文件中，jvm加载class文件之后，仍然存在。
// @Constraint —— 表示我们判断逻辑的具体实现类是什么。

// 我的第一个注解
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MyConstraintValidator.class)
public @interface MyConstraint {

	String message();

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}