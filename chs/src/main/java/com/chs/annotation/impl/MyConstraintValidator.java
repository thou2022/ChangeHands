package com.chs.annotation.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.chs.annotation.MyConstraint;

public class MyConstraintValidator implements ConstraintValidator<MyConstraint, Object> {

	//初始化
	@Override
	public void initialize(MyConstraint constraintAnnotation) {
		System.out.println("第一次使用时初始化");
	}

	//逻辑判断
	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		String name = (String) value;
		if (name.equals("tom")) {
			return true;
		}else {
			return false;
		}
	}

}