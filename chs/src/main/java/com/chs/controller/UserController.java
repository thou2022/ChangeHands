package com.chs.controller;

import com.chs.model.dto.UserDto;
import com.chs.service.NoticeService;
import com.chs.service.UserService;
import com.chs.util.ResultUtil;
import com.chs.util.TokenUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * (User)表控制层
 *
 * @author mopeiwen
 * @since 2020-06-10 10:49:15
 */
@RestController
//@RequestMapping("/user")
@Validated
public class UserController {
	/**
	 * 服务对象
	 */
	@Resource
	private UserService userService;
	@Resource
	private NoticeService noticeService;

	@Autowired
	private HttpServletRequest request;

	// 用户登录
	@PostMapping(value = "/login")
	public ResultUtil loginByAccount(@NotBlank(message = "用户账号不能为空") String username,
			@NotBlank(message = "密码不能为空") @Pattern(regexp = "^(?![a-zA-Z]+$)(?![0-9]+$)[A-Za-z0-9]{8,18}$", message = "密码必须为8-18为数字与字母组合") String password)
			throws UnsupportedEncodingException {
		System.out.println("用户：" + username + ",密码：" + password);
		return ResultUtil.ok("登录成功", userService.selectByAccount(username, password));
	}

	// 获得验证码 TODO 考虑网易云(20条)、乐信、互亿(66条)
	@PostMapping(value = "/get-code")
	public ResultUtil getCode(@NotBlank(message = "账号不能为空") String username) {
		return ResultUtil.ok("发送成功", userService.getCode(username));
	}

	// 用户注册
	@PostMapping(value = "/register")
	public ResultUtil registerAccount(@Valid @RequestBody UserDto user) {
		return ResultUtil.ok("注册成功", userService.insert(user));
	}

	// 找回密码
	@PostMapping(value = "/pwd-check-code")
	public ResultUtil pwdCheckCode(@RequestBody UserDto user) {
		return ResultUtil.ok("找回成功", userService.updatePassword(user));
	}

	// 手机或邮箱密码
	@PostMapping(value = "/user/check-code")
	public ResultUtil userCheckCode(@RequestBody UserDto user) {
		return ResultUtil.ok("修改成功", userService.updatePhoneOrEmail(user));
	}

	// 查看个人信息
	@GetMapping(value = "/user/sel-info")
	public ResultUtil userSelInfo() {
		return ResultUtil.ok("查询成功", userService.queryById(TokenUtil.getUserId(this.request.getHeader("Authorization"))));
	}

	// 修改个人信息
	@PostMapping(value = "/user/update-info")
	public ResultUtil userUpdateInfo(@RequestBody UserDto user) {
		return ResultUtil.ok("修改成功", userService.update(user.getUser()));
	}

	// 查看公告列表
	@GetMapping(value = "/user/sel-all-notice")
	public ResultUtil userSelAllNotice(int size) {
		return ResultUtil.ok("查询成功", noticeService.selectAllBySize(size));
	}

	// 查看公告列表
	@GetMapping(value = "/user/sel-notice")
	public ResultUtil userSelNotice(String nid) {
		return ResultUtil.ok("查询成功", noticeService.queryById(nid));
	}
}