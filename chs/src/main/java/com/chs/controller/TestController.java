package com.chs.controller;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.chs.model.User;
import com.chs.util.ResultUtil;

@RestController
@RequestMapping("/test")
public class TestController {

	@PostMapping(value = "/user")
	public void create(@Valid @RequestBody User user) {
		System.out.println(user);
	}

	@GetMapping(value = "/val")
	@ResponseBody
	public ResultUtil printf(String val) {
		return ResultUtil.ok("测试传值", val);
	}

	@PostMapping(value = "/obj")
	@ResponseBody
	public ResultUtil printfByObj(User user, String phone) {
		System.out.println(user);
		System.out.println(phone);
		return ResultUtil.ok("测试传值", user);
	}
}
