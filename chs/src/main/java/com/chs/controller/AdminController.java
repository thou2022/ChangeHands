package com.chs.controller;

import com.chs.model.Admin;
import com.chs.model.Notice;
import com.chs.model.Plate;
import com.chs.model.PlateContent;
import com.chs.model.PlateType;
import com.chs.model.Product;
import com.chs.model.ProductType;
import com.chs.model.User;
import com.chs.service.AdminService;
import com.chs.service.LoginHistoryService;
import com.chs.service.NoticeService;
import com.chs.service.OrderService;
import com.chs.service.PlateContentService;
import com.chs.service.PlateService;
import com.chs.service.PlateTypeService;
import com.chs.service.ProductCommentService;
import com.chs.service.ProductService;
import com.chs.service.ProductTypeService;
import com.chs.service.UserService;
import com.chs.util.ResultUtil;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.UnsupportedEncodingException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

/**
 * (Admin)表控制层
 *
 * @author mopeiwen
 * @since 2020-06-10 10:47:20
 */
@RestController
@RequestMapping("/admin")
public class AdminController {
	/**
	 * 服务对象
	 */
	@Resource
	private AdminService adminService;
	@Resource
	private LoginHistoryService loginHistoryService;
	@Resource
	private UserService userService;
	@Resource
	private ProductTypeService productTypeService;
	@Resource
	private ProductService productService;
	@Resource
	private OrderService orderService;
	@Resource
	private NoticeService noticeService;
	@Resource
	private ProductCommentService productCommentService;
	@Resource
	private PlateService plateService;
	@Resource
	private PlateTypeService plateTypeService;
	@Resource
	private PlateContentService plateContentService;

	/**
	 * 管理员登录
	 * 
	 * @param account
	 * @param password
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@PostMapping(value = "/login")
	public ResultUtil loginByAccount(@Valid Admin admin) throws UnsupportedEncodingException {
		return ResultUtil.ok("管理员登录成功", adminService.selectByAccount(admin.getAccount(), admin.getPassword()));
	}

	/**
	 * 查看管理员信息
	 * 
	 * @return
	 */
	@GetMapping(value = "/get-admin")
	public ResultUtil getAdmin() {
		return ResultUtil.ok("查询成功", adminService.selectById());
	}

	/**
	 * 修改管理员信息
	 * 
	 * @param admin
	 * @return
	 */
	@PostMapping(value = "/set-admin")
	public ResultUtil setAdmin(@RequestBody Admin admin) {
		return ResultUtil.ok("修改成功", adminService.update(admin));
	}

	/**
	 * 查看IO
	 * 
	 * @return
	 */
	@GetMapping(value = "/look-io")
	public ResultUtil getIoDto() {
		return ResultUtil.ok("管理员查询IO成功", adminService.getIoDto());
	}

	/* ===============用户管理模块 ==================== */
	/**
	 * 查询所有用户登录记录
	 * 
	 * @param pageNum
	 * @param pageSize
	 * @param model
	 * @return
	 */
	@GetMapping(value = "/login-historylist")
	public ResultUtil selectLoginHistory(@RequestParam(name = "pageNum", required = true, defaultValue = "1") int pageNum,
			@RequestParam(name = "pageSize", required = true, defaultValue = "4") int pageSize) {
		System.out.println(pageNum + " " + pageSize);
		return ResultUtil.ok("管理员查询所有登录记录成功", loginHistoryService.selectAll(pageNum, pageSize));
	}
	
	/**
	 * 模糊查询用户登录
	 */
	@GetMapping(value = "/find-user-gh")
	public ResultUtil findLoginHistoryByList(String gh) {
		return ResultUtil.ok("查询用户登录记录成功", adminService.findLoginHistoryByList(gh));
	}

	/**
	 * 根据登录记录ID删除记录
	 * 
	 * @param lhid
	 * @return
	 */
	@PostMapping(value = "/delete-loginhistory")
	public ResultUtil deleteLoginHistory(String lhid) {
		return ResultUtil.ok("管理员删除登录记录成功", loginHistoryService.deleteById(lhid));

	}
	
	/**
	 * 批量删除用户登录记录
	 * 
	 * @param ids
	 * @return
	 */
	@PostMapping(value = "/batch-delete-lh")
	public ResultUtil batchdeleteLoginHistory(String lhid) {
		return ResultUtil.ok("管理员批量删除用户登录记录成功",loginHistoryService.batchdeleteLoginHistory(lhid));
	}

	/**
	 * 查询所有用户列表
	 * 
	 * @param admin
	 * @return the ResultVo
	 */
	@GetMapping(value = "/select-userlist")
	public ResultUtil queryUserlist(@RequestParam(name = "pageNum", required = true, defaultValue = "1") int pageNum,
			@RequestParam(name = "pageSize", required = true, defaultValue = "4") int pageSize) {
		System.out.println(pageNum + " " + pageSize);
		return ResultUtil.ok("管理员查询所有用户列表成功", userService.selectAll(pageNum, pageSize));
	}

	/**
	 * 通过用户ID查询用户详细信息
	 * 
	 * @param id
	 * @return the ResultVo
	 */
	@GetMapping(value = "/select-user")
	public ResultUtil getUser(@RequestParam(name = "uid", required = false) String uid) {
		System.out.println(uid);
		return ResultUtil.ok("管理员查询用户详情成功", userService.queryById(uid));
	}

	/**
	 * 管理员添加用户信息
	 * 
	 * @param admin
	 * @return the ResultVo
	 */
	@PostMapping(value = "/add-user")
	public ResultUtil addUser(@Valid @RequestBody User user) {
		return ResultUtil.ok("管理员添加用户信息成功", userService.insert(user));
	}

	/**
	 * 管理员修改用户信息
	 * 
	 * @param admin
	 * @return the ResultVo
	 */
	@PostMapping(value = "/update-user")
	public ResultUtil updateUser(@Valid @RequestBody User user) {
		return ResultUtil.ok("管理员更新用户信息成功", userService.update(user));
	}

	/**
	 * 管理员根据用户ID删除用户信息
	 * 
	 * @param id
	 * @return the ResultVo
	 */
	@PostMapping(value = "/delect-user")
	public ResultUtil deleteUserById(String uid) {
		return ResultUtil.ok("管理员删除用户信息成功", userService.deleteById(uid));
	}

	/**
	 * 批量删除用户
	 * 
	 * @param ids
	 * @return
	 */
	@PostMapping(value = "/batch-delete-user")
	public ResultUtil deleteBatchUserById(String uid) {
		return ResultUtil.ok("管理员批量删除用户信息成功", userService.batchdeleteUser(uid));
	}

	/**
	 * 模糊查询用户
	 */
	@GetMapping(value = "/find-user-ss")
	public ResultUtil findUserByName(String ss) {
		return ResultUtil.ok("查询用户成功", adminService.findUserByList(ss));
	}

	/* ===============商品管理模块 ==================== */
	/**
	 * 管理员添加商品类型
	 * 
	 * @param productType
	 * @return the ResultVo
	 */
	@PostMapping(value = "/add-product-sort")
	public ResultUtil addProductType(@Valid @RequestBody ProductType productType) {
		return ResultUtil.ok("管理员添加商品类型成功", productTypeService.insert(productType));
	}

	/**
	 * 根据商品类型id删除商品分类
	 * 
	 * @param id
	 * @return the ResultVo
	 */
	@PostMapping(value = "/delete-product-sort")
	public ResultUtil deleteproductTypeByPtid(String ptid) {
		return ResultUtil.ok("管理员删除商品类型成功", productTypeService.deleteById(ptid));
	}

	/**
	 * 查询所有商品类型
	 * 
	 * @param productType
	 * @return the ResultVo
	 */
	@GetMapping(value = "/select-product-sort")
	public ResultUtil queryProductType(ProductType productType) {
		return ResultUtil.ok("管理员查询所有商品类型成功", productTypeService.selectAll(productType));
	}

	/**
	 * 修改商品类型分类
	 * 
	 * @param productType
	 * @return the ResultVo
	 */
	@PostMapping(value = "/update-product-sort")
	public ResultUtil updateProductType(@Valid @RequestBody ProductType productType) {
		return ResultUtil.ok("管理员更新商品分类成功", productTypeService.update(productType));
	}

	/**
	 * 管理员添加商品
	 * 
	 * @param product
	 * @return the ResultVo
	 */
	/*
	 * @PostMapping(value = "/add-product") public ResultVo addProduct(@RequestBody
	 * Product product) { return ResultVo.ok("管理员添加商品成功",
	 * productService.insert(product)); }
	 */
	@PostMapping(value = "/add-product")
	public ResultUtil addProduct(@RequestParam MultipartFile[] file, @Valid Product product, HttpServletRequest request,
			HttpServletResponse response) {
		System.out.println(product);
		return ResultUtil.ok("管理员添加商品成功", productService.insert(file, product, request, response));
	}

	/**
	 * 管理员根据商品id删除商品
	 * 
	 * @param id
	 * @return the ResultVo
	 */
	@PostMapping(value = "/delete-product")
	public ResultUtil deleteProductById(String pid) {
		return ResultUtil.ok("管理员删除商品成功", productService.deleteById(pid));
	}

	/**
	 * 批量删除商品
	 * 
	 * @param ids
	 * @return
	 */
	@PostMapping(value = "/batch-delect-product")
	public ResultUtil deleteBatchProductById(String pid) {
		return ResultUtil.ok("管理员批量删除商品信息成功", productService.batchdeleteProduct(pid));
	}

	/**
	 * 查询所有商品列表
	 * 
	 * @param admin
	 * @return the ResultVo
	 */
	@GetMapping(value = "/select-product-list")
	public ResultUtil queryProductlist(@RequestParam(name = "pageNum", required = true, defaultValue = "1") int pageNum,
			@RequestParam(name = "pageSize", required = true, defaultValue = "4") int pageSize) {
		return ResultUtil.ok("管理员查询所有商品列表成功", productService.selectAll(pageNum, pageSize));
	}

	/**
	 * 通过商品ID查询单条商品数据
	 * 
	 * @param id
	 * @return the ResultVo
	 */
	@GetMapping(value = "/select-product")
	public ResultUtil getProductById(String pid) {
		return ResultUtil.ok("管理员查询商品详情成功", productService.queryById(pid));
	}

	/**
	 * 管理员修改商品信息
	 * 
	 * @param admin
	 * @return the ResultVo
	 */
	@PostMapping(value = "/update-product")
	public ResultUtil updateProduct(@Valid @RequestBody Product product) {
		return ResultUtil.ok("管理员更新商品信息成功", productService.update(product));
	}

	/**
	 * 管理员审核商品
	 * 
	 * @param pid
	 * @return
	 */
	@PostMapping(value = "/examine-product")
	public ResultUtil examineProduct(@RequestBody Product product) {
		return ResultUtil.ok("管理员审核商品状态成功", productService.examineProduct(product));

	}
	
	/**
	 * 模糊查询商品
	 */
	@GetMapping(value = "/find-product-pp")
	public ResultUtil findProductByList(String pp) {
		return ResultUtil.ok("查询商品成功", adminService.findProductByList(pp));
	}

	/* ===============订单管理模块 ==================== */
	/**
	 * 管理员查看订单详情
	 * 
	 * @param id
	 * @return the ResultVo
	 */
	@GetMapping(value = "/select-order")
	public ResultUtil getOrderById(String oid) {
		return ResultUtil.ok("管理员查看订单详情成功", orderService.queryById(oid));
	}

	/**
	 * 管理员查看订单列表
	 * 
	 * @param order
	 * @return the ResultVo
	 */
	@GetMapping(value = "/seslect-order-notes-list")
	public ResultUtil queryOrderList(@RequestParam(name = "pageNum", required = true, defaultValue = "1") int pageNum,
			@RequestParam(name = "pageSize", required = true, defaultValue = "4") int pageSize) {
		return ResultUtil.ok("管理员查看订单列表成功", orderService.selectAll(pageNum, pageSize));
	}

	/**
	 * 管理员删除订单
	 * 
	 * @param id
	 * @return the ResultVo
	 */
	@PostMapping(value = "/delete-order")
	public ResultUtil deleteOrder(String oid) {
		return ResultUtil.ok("管理员删除订单成功", orderService.deleteById(oid));
	}

	/**
	 * 批量删除订单
	 * 
	 * @param ids
	 * @return
	 */
	@PostMapping(value = "/batch-delete-order")
	public ResultUtil deleteBatchOrderById(String oid) {
		return ResultUtil.ok("管理员批量删除订单信息成功", orderService.batchdeleteOrder(oid));
	}

	/**
	 * 模糊查询订单
	 */
	@GetMapping(value = "/find-order-oo")
	public ResultUtil findOrderByList(String oo) {
		return ResultUtil.ok("查询订单成功", adminService.findOrderByList(oo));
	}
	/* ===============公告管理模块 ==================== */
	/**
	 * 管理员添加公告
	 * 
	 * @param notice
	 * @return the ResultVo
	 */
	@PostMapping(value = "/add-notice")
	public ResultUtil addNotice(@Valid @RequestBody Notice notice) {
		return ResultUtil.ok("管理员添加公告成功", noticeService.insert(notice));
	}

	/**
	 * 管理员删除公告
	 * 
	 * @param id
	 * @return the ResultVo
	 */
	@PostMapping(value = "/delete-notice")
	public ResultUtil deleteNotice(String nid) {
		return ResultUtil.ok("管理员删除公告成功", noticeService.deleteById(nid));
	}

	/**
	 * 批量删除公告
	 * 
	 * @param ids
	 * @return
	 */
	@PostMapping(value = "/batch-delete-notice")
	public ResultUtil deleteBatchNoticeById(String nid) {
		return ResultUtil.ok("管理员批量删除公告信息成功", noticeService.batchdeleteNotice(nid));
	}

	/**
	 * 管理员查看公告详情
	 * 
	 * @param id
	 * @return the ResultVo
	 */
	@GetMapping(value = "/select-notice-details")
	public ResultUtil getNotice(String nid) {
		return ResultUtil.ok("管理员查看公告详情成功", noticeService.queryById(nid));
	}

	/**
	 * 管理员查看公告列表
	 * 
	 * @param notice
	 * @return the ResultVo
	 */
	@GetMapping(value = "/select-notice-list")
	public ResultUtil queryNoticeList(@RequestParam(name = "pageNum", required = true, defaultValue = "1") int pageNum,
			@RequestParam(name = "pageSize", required = true, defaultValue = "4") int pageSize) {
		return ResultUtil.ok("管理员查看公告列表成功", noticeService.selectAll(pageNum, pageSize));
	}

	/**
	 * 管理员修改公告
	 * 
	 * @param notice
	 * @return the ResultVo
	 */
	@PostMapping(value = "/update-notice")
	public ResultUtil updateNotice(@Valid @RequestBody Notice notice) {
		return ResultUtil.ok("管理员修改公告成功", noticeService.update(notice));
	}

	/* ===============评论管理模块 ==================== */
	/**
	 * 管理员查询评论列表
	 * 
	 * @param productComment
	 * @return the ResultVo
	 */
	@GetMapping(value = "/select-comment-list")
	public ResultUtil queryCommentList(@RequestParam(name = "pageNum", required = true, defaultValue = "1") int pageNum,
			@RequestParam(name = "pageSize", required = true, defaultValue = "4") int pageSize) {
		return ResultUtil.ok("管理员查询评论列表成功", productCommentService.selectAll(pageNum, pageSize));
	}

	/**
	 * 管理员删除评论
	 * 
	 * @param id
	 * @return the ResultVo
	 */
	@PostMapping(value = "/delete-comment")
	public ResultUtil deleteCommentById(String pcid) {
		return ResultUtil.ok("管理员删除评论成功", productCommentService.deleteById(pcid));
	}

	/**
	 * 批量删除评论
	 * 
	 * @param ids
	 * @return
	 */
	@PostMapping(value = "/batch-delete-comment")
	public ResultUtil deleteBatchProductCommentById(String pcid) {
		return ResultUtil.ok("管理员批量删除商品评论成功", productCommentService.batchdeleteComment(pcid));
	}
	
	/**
	 * 模糊查询商品评论
	 */
	@GetMapping(value = "/find-comment-cc")
	public ResultUtil findCommentByList(String cc) {
		return ResultUtil.ok("查询商品评论成功", adminService.findCommentByList(cc));
	}

	/* ===============板块管理模块 ==================== */
	/**
	 * 管理员添加板块信息
	 * 
	 * @param admin
	 * @return the ResultVo
	 */
	@PostMapping(value = "/add-plate")
	public ResultUtil addPlate(@RequestBody Plate plate) {
		return ResultUtil.ok("管理员添加板块信息成功", plateService.insert(plate));
	}

	/**
	 * 管理员删除板块
	 * 
	 * @param id
	 * @return the ResultVo
	 */
	@PostMapping(value = "/delete-plate")
	public ResultUtil deletePlateById(String pid) {
		return ResultUtil.ok("管理员删除板块成功", plateService.deleteById(pid));
	}

	/**
	 * 管理员查询所有板块
	 * 
	 * @param
	 * @return the ResultVo
	 */
	@GetMapping(value = "/select-first-plate")
	public ResultUtil queryAllPlate() {
		return ResultUtil.ok("管理员查询所有成功", plateService.selectAll());
	}

	/**
	 * 管理员查看首页板块详情
	 * 
	 * @param id
	 * @return the ResultVo
	 */
	@GetMapping(value = "/select-plate-details")
	public ResultUtil getPlateById(String pid) {
		System.out.println(pid);
		return ResultUtil.ok("管理员查看首页板块详情成功", plateService.queryById(pid));
	}

	/**
	 * 管理员修改板块
	 * 
	 * @param notice
	 * @return the ResultVo
	 */
	@PostMapping(value = "/update-plate")
	public ResultUtil updatePlate(@RequestBody Plate plate) {
		return ResultUtil.ok("管理员修改板块成功", plateService.update(plate));
	}

	/**
	 * 管理员添加板块类型信息
	 * 
	 * @param admin
	 * @return the ResultVo
	 */
	@PostMapping(value = "/add-platetype")
	public ResultUtil addPlateType(@RequestBody PlateType plateType) {
		return ResultUtil.ok("管理员添加板块类型信息成功", plateTypeService.insert(plateType));
	}

	/**
	 * 管理员删除板块类型
	 */
	@PostMapping(value = "/delete-platetype")
	public ResultUtil delectPlateTypeById(String pyid) {
		return ResultUtil.ok(" 管理员删除板块类型成功", plateTypeService.deleteById(pyid));

	}

	/**
	 * 管理员查询所有板块类型
	 * 
	 * @param plateType
	 * @return the ResultVo
	 */
	@GetMapping(value = "/select-platetype-list")
	public ResultUtil queryAllPlateType() {
		return ResultUtil.ok("管理员查询所有板块类型成功", plateTypeService.selectAll());
	}

	/**
	 * 管理员查看首页板块类型详情
	 * 
	 * @param id
	 * @return the ResultVo
	 */
	@GetMapping(value = "/select-platetype")
	public ResultUtil getPlateTypeById(String pyid) {
		return ResultUtil.ok("管理员查看首页板块类型详情成功", plateTypeService.queryById(pyid));
	}

	/**
	 * 管理员修改板块类型
	 * 
	 * @param plateType
	 * @return the ResultVo
	 */
	@PostMapping(value = "/update-plate-type")
	public ResultUtil updatePlateType(@RequestBody PlateType plateType) {
		return ResultUtil.ok("管理员修改板块成功", plateTypeService.update(plateType));
	}

	/**
	 * 管理员添加板块内容信息
	 * 
	 * @param plateContent
	 * @return the ResultVo
	 */
	@PostMapping(value = "/add-plate-content")
	public ResultUtil addPlateComment(@RequestBody PlateContent plateContent) {
		return ResultUtil.ok("管理员添加板块类型信息成功", plateContentService.insert(plateContent));
	}

	/**
	 * 管理员删除板块内容
	 */
	@PostMapping(value = "/delete-plate-content")
	public ResultUtil delectPlateComment(String pcid) {
		return ResultUtil.ok(" 管理员删除板块内容成功", plateContentService.deleteById(pcid));
	}

	/**
	 * 管理员查询板块内容列表
	 * 
	 * @param plateContent
	 * @return the ResultVo
	 */
	@GetMapping(value = "/select-plate-content-list")
	public ResultUtil queryPlateContentList(
			@RequestParam(name = "pageNum", required = true, defaultValue = "1") int pageNum,
			@RequestParam(name = "pageSize", required = true, defaultValue = "4") int pageSize) {
		return ResultUtil.ok("管理员查询板块内容列表成功", plateContentService.selectAll(pageNum, pageSize));
	}

	/**
	 * 管理员查看首页板块内容详情
	 * 
	 * @param id
	 * @return the ResultVo
	 */
	@GetMapping(value = "/select-plate-content")
	public ResultUtil getPlateContentById(String pcid) {
		return ResultUtil.ok("管理员查看首页板块内容详情成功", plateContentService.queryById(pcid));
	}

	/**
	 * 管理员修改板块内容
	 * 
	 * @param plateContent
	 * @return the ResultVo
	 */
	@PostMapping(value = "/update-plate-content")
	public ResultUtil updatePlateContent(@RequestBody PlateContent plateContent) {
		System.out.println(plateContent);
		return ResultUtil.ok("管理员修改板块内容成功", plateContentService.update(plateContent));
	}
}
