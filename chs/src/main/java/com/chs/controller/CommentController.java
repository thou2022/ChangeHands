package com.chs.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.chs.model.ProductComment;
import com.chs.model.dto.CommentDto;
import com.chs.service.OrderService;
import com.chs.service.ProductCommentService;
import com.chs.util.ResultUtil;

@RestController
@RequestMapping("/user")
public class CommentController {
	/**
	 * 服务对象
	 */
	@Resource
	private ProductCommentService productCommentService;
	@Resource
	private OrderService orderService;

	// 查看订单详情
	@GetMapping(value = "/sel-order")
	public ResultUtil selOrder(String oid) {
		return ResultUtil.ok("查询成功", orderService.queryById(oid));
	}

	// 查看订单列表
	@GetMapping(value = "/sel-orders")
	public ResultUtil selOrders(Integer pageNum, Integer pageSize, Integer flag) {
		System.out.println(pageNum + " " + pageSize + " " + flag);
		return ResultUtil.ok("查询成功", orderService.selectAll(pageNum, pageSize, flag));
	}

	// 删除订单
	@PostMapping(value = "/del-order")
	public ResultUtil delOrder(String oid) {
		return ResultUtil.ok("删除成功", orderService.deleteById(oid));
	}

	// 我的已卖记录(我的相关联订单)
	@GetMapping(value = "/sel-my-sold")
	public ResultUtil selMySold(Integer pageNum, Integer pageSize) {
		System.out.println(pageNum + " " + pageSize);
		return ResultUtil.ok("查询成功", orderService.selectAllMySold(pageNum, pageSize));
	}

	// 填写物流号 TODO 考虑物流接口
	@PostMapping(value = "/write-log-no")
	public ResultUtil writeLogNo(String oid, String logisticsNo) {
		System.out.println(oid + " " + logisticsNo);
		return ResultUtil.ok("修改成功", orderService.writeLogNo(oid, logisticsNo));
	}

	// 确认收货
	@PostMapping(value = "/ok-receipt")
	public ResultUtil okReceipt(String oid) {
		System.out.println(oid);
		return ResultUtil.ok("修改成功", orderService.okReceipt(oid));
	}

	// 查看商品的所有评论(商品页)
	@GetMapping(value = "/sel-comments")
	public ResultUtil getAllComment(@RequestParam(name = "pageNum", required = true, defaultValue = "1") int pageNum,
			@RequestParam(name = "pageSize", required = true, defaultValue = "4") int pageSize, String pid,
			Integer type, String pcid) {
		return ResultUtil.ok("查看成功", productCommentService.selectAll(pageNum, pageSize, pid, type, pcid));
	}

	// 查看自己的所有评论(包括商品评论和回复)
	@GetMapping(value = "/sel-my-comments")
	public ResultUtil getMyAllComment(@RequestParam(name = "pageNum", required = true, defaultValue = "1") int pageNum,
			@RequestParam(name = "pageSize", required = true, defaultValue = "4") int pageSize) {
		return ResultUtil.ok("查看成功", productCommentService.selectMyAll(pageNum, pageSize));
	}

	// 发布商品评论(多个商品和单个商品)
	@PostMapping(value = "/add-comment")
	public ResultUtil addComment(@Valid @RequestBody CommentDto commentDto) {
		return ResultUtil.ok("发布评论成功", productCommentService.insertComments(commentDto));
	}

	// 发布回复(对评论回复)
	@PostMapping(value = "/add-to-comment")
	public ResultUtil addToComment(@Valid @RequestBody ProductComment productComment) {
		return ResultUtil.ok("发布评论成功", productCommentService.insert(productComment));
	}

	// 增加单个图片
	@PostMapping(value = "/add-comment-file")
	public ResultUtil addProPicture(@RequestParam MultipartFile file, HttpServletRequest request,
			HttpServletResponse response) {
		return ResultUtil.ok("添加成功", productCommentService.insert(file, request, response));
	}

	// 用户删除评论
	@PostMapping(value = "/del-comment")
	public ResultUtil delComment(String pcid) {
		return ResultUtil.ok("删除成功", productCommentService.deleteById(pcid));
	}

}
