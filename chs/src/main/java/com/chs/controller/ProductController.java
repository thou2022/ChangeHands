package com.chs.controller;

import com.chs.model.Product;
import com.chs.service.ProductHistoryService;
import com.chs.service.ProductService;
import com.chs.service.ProductTypeService;
import com.chs.service.UserService;
import com.chs.util.ResultUtil;
import com.chs.util.TokenUtil;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

/**
 * (User)表控制层
 *
 * @author mopeiwen
 * @since 2020-06-10 10:49:15
 */
@RestController
@RequestMapping("/user")
public class ProductController {
	/**
	 * 服务对象
	 */
	@Resource
	private UserService userService;
	@Resource
	private ProductService productService;
	@Resource
	private ProductTypeService productTypeService;
	@Resource
	private ProductHistoryService productHistoryService;

	// 查看商品详情
	@GetMapping(value = "/sel-product")
	public ResultUtil selProduct(String pid) {
		return ResultUtil.ok("查询成功", productService.queryByIdForHistory(pid));
	}

	// 获得用户的商品浏览记录
	@GetMapping(value = "/browse-history")
	public ResultUtil browseHistory(@RequestParam(name = "pageNum", required = true, defaultValue = "1") int pageNum,
			@RequestParam(name = "pageSize", required = true, defaultValue = "4") int pageSize,
			HttpServletRequest request) {
		System.out.println(pageNum + " " + pageSize);
		return ResultUtil.ok("查询成功", productHistoryService.selectAll(pageNum, pageSize,
				TokenUtil.getUserId(request.getHeader("Authorization"))));
	}

	// 清空商品浏览记录
	@PostMapping(value = "/del-browse-history")
	public ResultUtil delBrowseHistory(HttpServletRequest request) {
		return ResultUtil.ok("删除成功",
				productHistoryService.deleteAll(TokenUtil.getUserId(request.getHeader("Authorization"))));
	}

	// 获得所有商品列表
	@GetMapping(value = "/sel-product-list")
	public ResultUtil selProductList(@RequestParam(name = "pageNum", required = true, defaultValue = "1") int pageNum,
			@RequestParam(name = "pageSize", required = true, defaultValue = "4") int pageSize) {
		System.out.println(pageNum + " " + pageSize);
		return ResultUtil.ok("查询成功", productService.selectAllForUser(pageNum, pageSize));
	}

	// 获得我的商品列表
	@GetMapping(value = "/sel-my-pro-list")
	public ResultUtil selMyProList(@RequestParam(name = "pageNum", required = true, defaultValue = "1") int pageNum,
			@RequestParam(name = "pageSize", required = true, defaultValue = "4") int pageSize,
			HttpServletRequest request) {
		System.out.println(pageNum + " " + pageSize);
		return ResultUtil.ok("查询成功",
				productService.selectAll(pageNum, pageSize, TokenUtil.getUserId(request.getHeader("Authorization"))));
	}

	// 通过类型获得商品列表
	@GetMapping(value = "/sel-pro-by-type")
	public ResultUtil getProByType(@RequestParam(name = "pageNum", required = true, defaultValue = "1") int pageNum,
			@RequestParam(name = "pageSize", required = true, defaultValue = "4") int pageSize, String ptid) {
		System.out.println(ptid);
		return ResultUtil.ok("查询成功", productService.selectAllByType(pageNum, pageSize, ptid));
	}

	// 获得商品类型
	@GetMapping(value = "/sel-pro-type")
	public ResultUtil getProType() {
		return ResultUtil.ok("查询成功", productTypeService.selectAll());
	}

	// 删除商品详情
	@PostMapping(value = "/del-product")
	public ResultUtil delProduct(String pid) {
		System.out.println(pid);
		return ResultUtil.ok("删除成功", productService.deleteById(pid));
	}

	// TODO 考虑删除原来的图片
	// 增加单个图片
	@PostMapping(value = "/add-pro-picture")
	public ResultUtil addProPicture(@RequestParam MultipartFile file, HttpServletRequest request,
			HttpServletResponse response) {
		// System.out.println(file.getName());
		return ResultUtil.ok("添加成功", productService.insertFile(file, request, response));
	}

	// 增加商品
	@PostMapping(value = "/add-product")
	public ResultUtil addProduct(@Valid @RequestBody Product product) {
		System.out.println(product);
		return ResultUtil.ok("添加成功", productService.insertForUid(product));
	}

	// 增加商品,包括图片 已放弃
	@PostMapping(value = "/add-pro-file")
	public ResultUtil addProFile(@RequestParam MultipartFile[] file, Product product, HttpServletRequest request,
			HttpServletResponse response) {
		System.out.println(product);
		return ResultUtil.ok("添加成功", productService.insert(file, product, request, response));
	}

	// 修改商品
	@PostMapping(value = "/update-product")
	public ResultUtil updateProduct(@RequestBody Product product) {
		System.out.println(product);
		return ResultUtil.ok("修改成功", productService.update(product));
	}

	// 修改商品,包括图片 已放弃
	@PostMapping(value = "/update-pro-file")
	public ResultUtil updateProFile(@RequestParam MultipartFile[] file, Product product, HttpServletRequest request,
			HttpServletResponse response) {
		System.out.println(product);
		return ResultUtil.ok("修改成功", productService.updateProFile(file, product, request, response));
	}
}
