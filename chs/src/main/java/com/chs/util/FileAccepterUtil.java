package com.chs.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;

/**
 * <p>
 * Description: 接收文件的目录管理器，读取fileAccepter.properties配置文件的配置信息，生成文件的目录及文件名
 * </p>
 * 
 * @author HiTMT
 * @date 2019-11-19 15:26:34
 */
public class FileAccepterUtil {
	private static Map<String, String> propertiesMap = new HashMap<String, String>();

	static {
		Properties prp = new Properties();
		InputStream inputStream = null;
		String path = "/fileAccepter.properties";
		try {
			inputStream = FileAccepterUtil.class.getResourceAsStream(path);
			BufferedReader bf = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
			prp.load(bf);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Set<Entry<Object, Object>> entrySet = prp.entrySet();
		Iterator<Entry<Object, Object>> iterator = entrySet.iterator();
		while (iterator.hasNext()) {
			Entry<Object, Object> entry = iterator.next();
			propertiesMap.put((String) entry.getKey(), (String) entry.getValue());
		}
	}

	/**
	 * 通过keysValues参数拿到URI
	 * 
	 * @param keysValues
	 *            keysValues.put("into_year", 2019)
	 * @return
	 * @throws Exception
	 */
	public static String getURI(Map<String, String> keysValues) throws Exception {
		// 获取配置文件中指定的URI
		String sourceURI = propertiesMap.get("bishe_preparation");
		// 将变量值替换
		String uri = variableReplace(sourceURI, keysValues);
		return uri;
	}

	/**
	 * 根据配置文件指定的路径和所给的文件名创建路径，并将拥有路径的空文件返回
	 * 
	 * @param path
	 *            存储文件的根目录
	 * @param fileFullName
	 *            文件的初始全名
	 * @param keysValues
	 *            配置文件中指定的变量参数
	 * @return 配置了文件名及路径的文件
	 * @throws Exception
	 */
	public static File getFile(String path, String fileFullName, Map<String, String> keysValues) throws Exception {
		if (!keysValues.containsKey("filename")) { // 如果未指定原文件名的参数就补充到keysValues中
			keysValues.put("filename", getFileNameExcludingEnds(fileFullName));
		}
		// 获取配置文件中指定的URI
		String sourceURI = propertiesMap.get("bishe_preparation");
		// 将变量值替换
		String uri = variableReplace(sourceURI, keysValues);
		File file = null;
		if (isDir(uri)) { // 在指定目录下存放文件，不格式化文件名
			file = new File(path+"/"+uri + fileFullName);
		} else { // 在指定目录下存放文件，并且格式化文件名
			file = new File(path+"/"+uri + getFileNameEnds(fileFullName));
		}
		// 创建目录
		System.setProperty("sun.jnu.encoding", "utf-8");
		// File dir=new
		// File(System.getProperty("user.dir")+"/"+uri.substring(0,uri.lastIndexOf('/')));
		File dir = new File(path + "/" + uri.substring(0, uri.lastIndexOf('/')));
		dir.mkdirs();
		// file.createNewFile();
		return file;
	}

	// 将含变量标记的原始uri替换为变量的当前值，并进行转义处理
	private static String variableReplace(String sourceURI, Map<String, String> keysValues) throws Exception {
		String uri = "";
		String[] ss = sourceURI.split("(?<!\\\\)[\\{\\}]"); // 按变量标记符成对拆解
		// 判断uri的变量标记成对拆解是否有错
		int addEvenTag = 1;
		if (sourceURI.lastIndexOf('}') >= 0) {
			addEvenTag = 0;
		}
		if ((ss.length - addEvenTag) % 2 != 0) { // 错误
			throw new Exception("配置的目录或文件解析错误：sourceURI=" + sourceURI);
		}
		for (int i = 0; i < ss.length; i++) {
			if (i % 2 == 1) {
				ss[i] = keysValues.get(ss[i]); // 将每个标记的变量替换为当前的值
			}
			uri += ss[i];
		}
		// 删除转义标记字符，即有效转义的"\"字符
		uri = deleteEscapeTags(uri);
		return uri;
	}

	// 判断配置文件指定的URI是否是仅仅指定路径
	private static boolean isDir(String sourceURI) {
		int len = sourceURI.length();
		if (len == 0) {
			return true;
		}
		return sourceURI.lastIndexOf("/") == len - 1;
	}

	// 删除转义标记，留下转义的目标字符
	private static String deleteEscapeTags(String s) {
		char[] cs = s.toCharArray();
		s = "";
		for (int i = 0; i < cs.length; i++) {
			if (cs[i] == '\\') { // 遇见转义字符'\'
				if (i + 1 > cs.length && cs[i + 1] != '\\') { // 认定当前'\'为无效字符
					i++;
				} else { // 认定当前'\'为有效字符，并将转义的目标字符加入s字符串
					i++;
					s += cs[i];
				}
			} else { // 非转义字符，直接加入s字符串
				s += cs[i];
			}
		}
		return s;
	}

	// 获取文件名后缀，包括"."
	public static String getFileNameEnds(String fileFullName) {
		int lastIdx = fileFullName.lastIndexOf(".");
		if (lastIdx < 1) {
			return "";
		} else {
			return fileFullName.substring(lastIdx);
		}
	}

	// 获取不包括后缀的文件名
	public static String getFileNameExcludingEnds(String fileFullName) {
		int lastIdx = fileFullName.lastIndexOf(".");
		if (lastIdx < 1) {
			return fileFullName;
		} else {
			return fileFullName.substring(0, lastIdx);
		}
	}
	
	//判断文件是否存在
	public static boolean isNotFile(String path) {
		File f = new File(path);
        if (f.exists()) {
			return true;
		}else{
			return false;
		}
	}
}
