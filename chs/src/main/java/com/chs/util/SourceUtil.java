package com.chs.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;

/**
 * 获得set的配置文件
 * @author: mopeiwen
 * @date: 2020年6月13日 下午2:53:29
 */
public class SourceUtil {
	private static Map<String, String> propertiesMap = new HashMap<String, String>();

	static {
		Properties prp = new Properties();
		InputStream inputStream = null;
		String path = "/set.properties";
		try {
			inputStream = FileAccepterUtil.class.getResourceAsStream(path);
			BufferedReader bf = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
			prp.load(bf);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Set<Entry<Object, Object>> entrySet = prp.entrySet();
		Iterator<Entry<Object, Object>> iterator = entrySet.iterator();
		while (iterator.hasNext()) {
			Entry<Object, Object> entry = iterator.next();
			propertiesMap.put((String) entry.getKey(), (String) entry.getValue());
		}
	}

	public static Map<String, String> getPropertiesMap() {
		return propertiesMap;
	}

	public static void setPropertiesMap(Map<String, String> propertiesMap) {
		SourceUtil.propertiesMap = propertiesMap;
	}
	
	public static String getPictureUrl() {
		return propertiesMap.get("picture.url");
	}

}
