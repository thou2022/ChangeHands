package com.chs.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 读写txt文件
 * 
 * @author mopeiwen
 * @version 2020年2月23日 上午2:04:13
 */

public class TXTUtil {

	/**
	 * <p>
	 * Description: 使用指定分隔符拼接字符串list
	 * </p>
	 * 
	 * @author 蓝伟格 @date 2020-03-01 01:41:07
	 */
	public static String jointStringList(List<String> list, String separator) {
		StringBuffer strb = new StringBuffer();
		Iterator<String> it = list.iterator();
		while (it.hasNext()) {
			strb.append(separator);
			strb.append(it.next());
		}
		String s = null;
		if (strb.length() >= separator.length()) {
			s = strb.substring(separator.length());
		}
		return s;
	}

	/**
	 * 按特定字符分割字符串，并在<之前>拼接参数返回
	 * @param val 字符串
	 * @param split 分割符
	 * @param splice 拼接参数
	 */
	public static String splitAndSplice(String val,String split,String splice) {
		
		String[] arr =  arrayUnique(valueToStr(val,split));
		
		String vString = "";
		for (String ss : arr) {
			String vs = splice + ss + split;
			vString = vString + vs;
		}
		
		if (!split.equals("")) {
			vString = vString.substring(0, vString.length() - 1);
		}
		return vString;
		
	}
	
	/**
	 * 字符串去重复
	 * 
	 * @param ss
	 * @return
	 */
	public static String[] arrayUnique(String[] ss) {
		List<String> list = new ArrayList<String>();
		for (String s : ss) {
			if (!list.contains(s)) // 或者list.indexOf(s)!=-1
				list.add(s);
		}
		return list.toArray(new String[list.size()]);
	}

	/**
	 * null转""
	 * 
	 * @param i
	 * @return
	 */
	public static Object nullToCharacter(Object i) {
		if (i == null) {
			i = "";
		}
		return i;
	}

	/**
	 * 把配置文件转Map
	 * 
	 * @param txtPath 文件路径
	 * @return maps
	 */
	public static Map<String, String> iniToMaps(String txtPath) {
		String[] str = readToString(txtPath);

		Map<String, String> maps = new HashMap<String, String>();

		for (String value : str) {
			String[] s = valueToStr(value, "=");
			maps.put(s[0], s[1]);
		}

		return maps;
	}

	/**
	 * 按特定符号拼接数组,如不需要字符,mark输入""即可
	 * 
	 * @param str  字符串数组
	 * @param mark 拼接字符
	 * @return
	 */
	public static String strToValue(String[] str, String mark) {
		if (str == null) {// 如为null返回null
			return null;
		}

		String value = "";

		for (String s : str) {

			if (s == null || s.equals("")) {
				continue;
			}

			value = value + s + mark;
		}

		if (!mark.equals("")) {
			value = value.substring(0, value.length() - 1);
		}

		return value;
	}

	/**
	 * 按规定字符分割字符串
	 * 
	 * @param value 字符串
	 * @param mark  字符
	 * @return 去除空值的数组
	 */
	public static String[] valueToStr(String value, String mark) {
		String[] str = value.split(mark);
		return deleteArrayNull(str);
	}

	/**
	 * 去除String数组中的空值
	 * 
	 * @param string
	 * @return
	 */
	public static String[] deleteArrayNull(String string[]) {
		String strArr[] = string;

		// step1: 定义一个list列表，并循环赋值
		ArrayList<String> strList = new ArrayList<String>();
		for (int i = 0; i < strArr.length; i++) {
			strList.add(strArr[i]);
		}

		// step2: 删除list列表中所有的空值
		while (strList.remove(null))

		while (strList.remove(""))

		if (strList.size() == 0) {
			return null;
		}

		// step3: 把list列表转换给一个新定义的中间数组，并赋值给它
		String strArrLast[] = strList.toArray(new String[strList.size()]);

		return strArrLast;
	}

	/**
	 * 传入txt路径读取txt文件
	 * 
	 * @param txtPath
	 * @return 返回读取到的内容
	 */
	public static String readTxt(String txtPath) {
		File file = new File(txtPath);
		if (file.isFile() && file.exists()) {
			try {
				FileInputStream fileInputStream = new FileInputStream(file);
				InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
				@SuppressWarnings("resource")
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

				StringBuffer sb = new StringBuffer();
				String text = null;
				while ((text = bufferedReader.readLine()) != null) {
					sb.append(text);
				}
				return sb.toString();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * 使用FileOutputStream来写入txt文件
	 * 
	 * @param txtPath txt文件路径
	 * @param content 需要写入的文本
	 */
	public static void writeTxt(String txtPath, String content) {
		FileOutputStream fileOutputStream = null;
		File file = new File(txtPath);
		try {
			if (file.exists()) {
				// 判断文件是否存在，如果不存在就新建一个txt
				file.createNewFile();
			}
			fileOutputStream = new FileOutputStream(file);
			fileOutputStream.write(content.getBytes());
			fileOutputStream.flush();
			fileOutputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 读取filePath的文件，将文件中的数据按照行读取到String数组中
	 * 
	 * @param filePath 文件的路径
	 * @return 文件中一行一行的数据,已处理空值
	 */
	public static String[] readToString(String filePath) {
		File file = new File(filePath);
		Long filelength = file.length(); // 获取文件长度
		byte[] filecontent = new byte[filelength.intValue()];
		try {
			FileInputStream in = new FileInputStream(file);
			in.read(filecontent);
			in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String[] fileContentArr = new String(filecontent).split("\r\n|\n");

		return deleteArrayNull(fileContentArr);// 返回文件内容,默认编码
	}

	/**
	 * 判断字符串是否在字符串数组内
	 * 
	 * @param str   字符串数组
	 * @param value 字符串
	 * @return
	 */
	public static boolean isContextByStr(String[] str, String value) {
		for (String s : str) {
			if (value.equals(s)) {
				return true;
			}
		}
		return false;
	}

}
