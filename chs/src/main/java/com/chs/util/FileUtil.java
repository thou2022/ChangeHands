package com.chs.util;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

/** 
* 文件操作处理工具类
* 
* @author mopeiwen
* @version 2020年2月23日 上午12:44:32 
*/
public class FileUtil {

	/**
	 * 上传文件处理,spring专属
	 * @param file
	 * @param request
	 * @param response
	 * @param asName 文件别名
	 * @param rootPath 子目录,开头已有"/"
	 * @return 上传后的文件
	 */
	public static File upFilebySpring(MultipartFile file,HttpServletRequest request,HttpServletResponse response,String asName,String rootPath) {

		FileUtil.createDirectory(request,response,rootPath); // 创建目录

		String fileFullName = file.getOriginalFilename(); // 获取文件名全称
		
		String path = request.getServletContext().getRealPath("/") + rootPath + "/" + asName + FileUtil.getFileNameEnds(fileFullName); // 保存文件的路径

		System.out.println(path);
		
		try {
			file.transferTo(new File(path));
		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
			return null;
		}

		return new File(path);
	}
	
	/**
	 * 在request.getServletContext().getRealPath目录下创建子目录
	 * @param request
	 * @param response
	 * @param rootPath 子目录,开头已有"/"
	 * @return
	 */
	public static void createDirectory(HttpServletRequest request,HttpServletResponse response,String rootPath){
		System.setProperty("sun.jnu.encoding", "utf-8");//创建目录,解决中文乱码
		File dir = new File(request.getServletContext().getRealPath("/")+rootPath);
		dir.mkdirs();
	}
	
	/**
	 * 获取文件名后缀，包括"."
	 * @param fileFullName
	 * @return
	 */
	public static String getFileNameEnds(String fileFullName) {
		int lastIdx = fileFullName.lastIndexOf(".");
		if (lastIdx < 1) {
			return "";
		} else {
			return fileFullName.substring(lastIdx);
		}
	}

	/**
	 * 获取不包括后缀的文件名
	 * @param fileFullName
	 * @return
	 */
	public static String getFileNameExcludingEnds(String fileFullName) {
		int lastIdx = fileFullName.lastIndexOf(".");
		if (lastIdx < 1) {
			return fileFullName;
		} else {
			return fileFullName.substring(0, lastIdx);
		}
	}
	
	/**
	 * 判断文件是否存在
	 * @param path
	 * @return
	 */
	public static boolean isNotFile(String path) {
		File f = new File(path);
        if (f.exists()) {
			return true;
		}else{
			return false;
		}
	}
}
