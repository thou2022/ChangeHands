package com.chs.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

/**
 * IP和终端获取工具
 * @author: mopeiwen
 * @date: 2020年6月20日 下午1:44:49
 */
public class IpUtil {

	/**
	 * 获取客户端IP地址
	 * @param request
	 * @return
	 */
	public static String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
			if (ip.equals("127.0.0.1")) {
				// 根据网卡取本机配置的IP
				InetAddress inet = null;
				try {
					inet = InetAddress.getLocalHost();
				} catch (UnknownHostException e) {
					e.printStackTrace();
				}
				ip = inet.getHostAddress();
			}
		}
		// 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
		if (ip != null && ip.length() > 15) {
			if (ip.indexOf(",") > 0) {
				ip = ip.substring(0, ip.indexOf(","));
			}
		}
		return ip;
	}
	
	public static String getClientOS(HttpServletRequest request){
		String userAgent = request.getHeader("user-agent");
        String cos = "unknow os";
        
        Pattern p = Pattern.compile(".*(Windows NT 6\\.1).*");
        Matcher m = p.matcher(userAgent);
        if(m.find()){
            cos = "Windows7";
            return cos;
        }
        
        p = Pattern.compile(".*(Windows NT 5\\.1|Windows XP).*");
        m = p.matcher(userAgent);
        if(m.find()){
            cos = "WinXP";
            return cos;
        }
        
        p = Pattern.compile(".*(Windows NT 5\\.2).*");
        m = p.matcher(userAgent);
        if(m.find()){
            cos = "Win2003";
            return cos;
        }
        
        p = Pattern.compile(".*(Win2000|Windows 2000|Windows NT 5\\.0).*");
        m = p.matcher(userAgent);
        if(m.find()){
            cos = "Win2000";
            return cos;
        }
        
        p = Pattern.compile(".*(Mac|apple|MacOS8).*");
        m = p.matcher(userAgent);
        if(m.find()){
            cos = "MAC";
            return cos;
        }
        
        p = Pattern.compile(".*(WinNT|WindowsNT).*");
        m = p.matcher(userAgent);
        if(m.find()){
            cos = "WinNT";
            return cos;
        }
        
        p = Pattern.compile(".*Linux.*");
        if(m.find()){
            cos = "Linux";
            return cos;
        }
        
        p = Pattern.compile(".*68k|68000.*");
        m = p.matcher(userAgent);
        if(m.find()){
            cos = "Mac68k";
            return cos;
        }
        
        p = Pattern.compile(".*(9x 4.90|Win9(5|8)|Windows 9(5|8)|95/NT|Win32|32bit).*");
        m = p.matcher(userAgent);
        if(m.find()){
            cos = "Win9x";
            return cos;
        }
        
        return cos;
    }
}
